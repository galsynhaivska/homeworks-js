/* При загрузке страницы - показать на ней кнопку с текстом “Нарисовать круг”.
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью JavaScript.
 - При нажатии на кнопку “Нарисовать круг” показывать одно поле ввода - диаметр круга. 
 - При нажатии на кнопку “Нарисовать” создать на странице 100 кругов (10 * 10) случайного цвета.
 - При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
то есть все остальные круги сдвигаются влево.
*/

//!!! тут код для малювання одного кола divs !!!

function createElement(nameEl, classEl, typeEl){
    const elem = document.createElement(nameEl);
    if(classEl !== "" || classEl !== null){
        elem.className = classEl;
    }
    if(typeEl !== "" || typeEl !== null){
        elem.type = typeEl;
    }
       return elem;
}


let inputDraw = createElement("input","input-text", "text"),
    btnDraw = createElement("button", "btn-draw", "button"),
    divs = createElement("div", "div-cycle", "");
btnDraw.textContent = "Намалювати";
inputDraw.placeholder = "Задайте діаметр круга";

btnStart = document.getElementById("start");

 
btnStart.onclick = () => {
   btnStart.remove(); 
   document.body.append(inputDraw);
   document.body.append(btnDraw);
   inputDraw.value ="";
   inputDraw.placeholder = "Задайте діаметр круга";
   inputDraw.focus();
}

btnDraw.onclick = () =>{
    if(parseFloat(inputDraw.value) - inputDraw.value !== 0 ){
        alert("Це не число! Спробуйте ще раз!");
        inputDraw.value ="";
        inputDraw.placeholder = "Задайте діаметр круга";
        inputDraw.focus();
    }
    else if(parseFloat(inputDraw.value) <= 0){
        alert("Введіть число > 0!");
        inputDraw.value ="";
        inputDraw.placeholder = "Задайте діаметр круга";
        inputDraw.focus();
    }
    else{
        inputDraw.remove();
        btnDraw.remove(); 
        console.log(parseFloat(inputDraw.value));
        divs.style.backgroundColor = `hsl(${Math.floor(Math.random()*360)}, 100%, 50%)`;
        divs.style.width = `${parseFloat(inputDraw.value)}px`;
        divs.style.height = `${parseFloat(inputDraw.value)}px`;
        divs.style.borderRadius = '50%';
        document.body.append(divs);
    }
}  

divs.onclick = () =>{
    divs.remove();
//btnStart = document.getElementById("start");
    document.body.append(btnStart);
}
        


import {items} from "./data-item.js";
import {setPageName, setSelectGroup, countCarts} from "./function.js";

//вибір і завантаження випадково обраних 4-х товарів //на головну сторінку 

//пам'ятаємо, що на головній сторінці
setPageName();     
window.sessionStorage.productGroup = "";
setSelectGroup();
//елементи для карточки товара головній сторінці
const [...divName] = document.querySelectorAll(".card-title"),
    [...imgItem] = document.querySelectorAll(".img-card"),
    [...priceItem] = document.querySelectorAll(".price-item");
const [...cardsItem] = document.querySelectorAll(".card");

//формуємо масив - cartsName - назв товарів
let cartsName = [];
let cart = {};   
let outputCarts = [];
items.forEach(elem => {        
    if(cart.itemName !== elem.itemName){
        cart = elem;
        cartsName.push(cart);
    }
}); 
    
//вибір і завантаження випадково обраних 4-х товарів 
for(let i = 1; i <= 4; i++){  //повтор 4 рази
    // генеруємо випадковий індекс
    let rand = Math.floor(Math.random()*cartsName.length); 
    outputCarts.push(cartsName[rand]); //додаємо
    //видаляємо обраний товар з cartsName, щоб не було повторів
    cartsName.splice(rand, 1);      
};
// заповнюємо картки товара з outputCarts
for(let i = 0; i <= 3; i++){
    cardsItem.id = i;
    divName[i].innerText = outputCarts[i].itemName; 
    imgItem[i].src = outputCarts[i].imageFront;
    imgItem[i].alt = outputCarts[i].itemName;
    priceItem[i].innerText = `$${parseFloat(outputCarts[i].price).toFixed(2)}`;
} 
//зберігаємо в sessionStorage
window.sessionStorage.carts = JSON.stringify(outputCarts);
    
//беремо дані по корзинці -userCard- з localStorage
let counterItem; 
let userCard =[];
const divCounterCard = document.querySelector(".counter-shopping-cart");
if(!window.localStorage.userCard){
    userCard = [];
    counterItem = 0; 
}else{
    userCard = JSON.parse(window.localStorage.userCard);
    counterItem = userCard.length;
   
}
divCounterCard.innerText = counterItem;
//------------------------------------------------

const [...addToCard] = document.querySelectorAll(".card .btn");
outputCarts = JSON.parse(window.sessionStorage.carts);

countCarts(cardsItem); //функція countCarts дає id карточкам
let currentItem = [];
//click on button "Add to Cart"
addToCard.forEach(elem =>{
    elem.addEventListener("click", (e) =>{
        console.log(elem.parentElement.id)
        e.preventDefault();
        //looking for item in userCard
        currentItem = outputCarts[elem.parentElement.id];
        if(userCard.some(item => item.idItem === currentItem.idItem 
            && item.color === currentItem.color 
            && item.sizeItem === currentItem.sizeItem)){
        const index = userCard.findIndex(item => item.idItem === currentItem.idItem 
            && item.color === currentItem.color 
            && item.sizeItem === currentItem.sizeItem);
            console.log(index);  
            userCard[index].quantity =  parseInt(userCard[index].quantity) + 1;
            console.log( userCard[index].quantity);
    }else{    
        userCard.push(currentItem);
        userCard[userCard.length - 1].quantity = 1;
    }     
        window.localStorage.userCard = JSON.stringify(userCard); 
        counterItem = userCard.length;
        divCounterCard.innerText = counterItem;
    });
});
//click on image
[...document.querySelectorAll(".card > a")].forEach(el =>{
    el.addEventListener("click", (ev) =>{  
        console.log(ev.target);
        console.log(outputCarts[el.parentElement.id]);
        const filterItem = items.filter(item => item.idItem === outputCarts[el.parentElement.id].idItem)
        console.log(outputCarts[el.parentElement.id].idItem);
        console.log(filterItem);
        window.sessionStorage.productGroup = filterItem[0].category;
        window.sessionStorage.selectItem = JSON.stringify(filterItem);
    });
});
//click on button 'Show all pants' or  'Show all shorts'
[...document.querySelectorAll(".banner-btn")].forEach(elem =>{
    elem.addEventListener("click", () =>{   
        window.sessionStorage.pageName = "./catalog/index.html";
        if(elem.innerText.includes("SHORTS")){
            window.sessionStorage.productGroup = "SHORTS";
        }else{
        window.sessionStorage.productGroup = "PANTS";
        }    
    });
});

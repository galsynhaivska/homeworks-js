class User{
    constructor(userId, userName, userEmail, userPassword){
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }
  
    setEmail(obj){
        console.dir(obj)
        document.querySelector(".user-email").innerText = "";
        obj.nextElementSibling.innerText = "";
        if(/^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/.test(this.userEmail)){     
            document.querySelector(".user-email").insertAdjacentHTML("beforeend", `<p>You set email: ${this.userEmail}</p>`)
            removeClass(obj, "wrong");   
        }else{
            addClass(obj, "wrong")
            modalDiv(obj.nextElementSibling, "Not correct email!");
        }
    }

    setPassword(obj){   
       document.querySelector(".user-password").innerText = "";
       obj.nextElementSibling.innerText = "";
       if(/[A-я_0-9]/.test(this.userPassword) && this.userPassword.length <= 16 && this.userPassword.length >= 8 ){
            document.querySelector(".user-password").insertAdjacentHTML("beforeend", `<p>You set password: ${this.userPassword}</p>`)
            removeClass(obj, "wrong");            
        }else{
            addClass(obj, "wrong")
            if(this.userPassword.length >= 16) {
                modalDiv(obj.nextElementSibling, "Password exceeds of 16 symbols!");
            }else if( this.userPassword.length <= 8){
                    modalDiv(obj.nextElementSibling, "Password is less then 8 symbols!");
            }else{   
            modalDiv(obj.nextElementSibling, "Not correct password!");
            }
        }    
    }
}

class ShowUserData{   //response for output data
    constructor(user){
        this.user = user
    }
    getId() {
        return ` <div class="item-user"> <div class="label">ID:</div><div>${this.user.userId}</div></div> `
        
    }    
    getName() {
        return ` <div class="item-user"><div class="label"> Name:</div><div> ${this.user.userName}</div></div> `
        
    }    
    getEmail() {
        return ` <div class="item-user"> <div class="label">Email:</div><div> ${this.user.userEmail}</div></div> `
        
    }    
}


function addClass(el, classEl){
    if(!el.classList.contains(classEl)){
        el.classList.add(classEl);
    }
}

function removeClass(el, classEl){
    if(el.classList.contains(classEl)){
        el.classList.remove(classEl);
    }
}

function modalDiv(el, text){
    console.log(el, text)
    return  el.innerText = text;
}


function createUserCard(obj, classObj){
    const newDiv = document.createElement(obj);
    newDiv.className = classObj;
    return newDiv;
}


//test-1
const btnOk = document.querySelector(".btn")

btnOk.addEventListener("click", (ev) =>{
    ev.preventDefault();
    const user = new User();
    user.userId = document.querySelector("#user-id").value;
    user.userName = document.querySelector("#name").value;
    user.userEmail = document.querySelector("#email").value;
    user.userPassword = document.querySelector("#password").value;
    console.log(user);
    user.setEmail(document.querySelector("#email"));
    user.setPassword(document.querySelector("#password"));
});



//test-2
const user2 = new ShowUserData(new User("2", "Lucy", "Lucky@gmail.com", "lurto4567"));

const container = document.querySelector(".container");
let userCard = createUserCard("div", "user-data");
container.append(userCard);
userCard.insertAdjacentHTML("beforeend", user2.getId());
document.querySelector(".user-data" ).insertAdjacentHTML("beforeend", user2.getName());
document.querySelector(".user-data").insertAdjacentHTML("beforeend", user2.getEmail());


const user1 = new ShowUserData(new User("1", "Ivan", "ivan@gmail.com", "iv3233"));
userCard = createUserCard("div", "user-data");
container.append(userCard);

userCard.insertAdjacentHTML("beforeend", user1.getId());
userCard.insertAdjacentHTML("beforeend", user1.getName());
userCard.insertAdjacentHTML("beforeend", user1.getEmail());





import {createElement} from "./function.js";

export function createCommentCart(dataComment){
    const maxBall = 5;
    const divReviews = document.querySelector(".container-reviews");
    const divComment = createElement("div", "item-reviews");
    const itemBalls = createElement("div", "item-balls");
    const titleComfort = createElement("h3", "item-balls > h3", "Comfort");
    const titleFit = createElement("h3", "item-balls > h3", "Fit");
    const titleOverall = createElement("h3", "item-balls > h3", "Overall");
    const titleQuality = createElement("h3", "item-balls > h3", "Quality");

 //fill properties by stars
    itemBalls.append(titleComfort);
    const  divStarsComfort =  createElement("div", "container-stars");
    createBalls(Math.floor(dataComment.comfort), divStarsComfort);
    itemBalls.append(divStarsComfort);

    itemBalls.append(titleFit);
    const  divStarsFit =  createElement("div", "container-stars");
    createBalls(Math.floor(dataComment.fit), divStarsFit);
    itemBalls.append(divStarsFit);

    itemBalls.append(titleOverall);
    const  divStarsOverAll =  createElement("div", "container-stars");
    createBalls(Math.floor(dataComment.overall),  divStarsOverAll);
    itemBalls.append(divStarsOverAll);

    itemBalls.append(titleQuality);
    const  divStarsQuality =  createElement("div", "container-stars");
    createBalls(Math.floor(dataComment.quality),  divStarsQuality);
    itemBalls.append(divStarsQuality);
    //add itemBalls to divComment
    divComment.append(itemBalls);
    const itemText = createElement("div", "item-text");
    const textComment = createElement("p", "item-text > p", dataComment.comment);
    itemText.append(textComment);
    const commentData = `By ${dataComment.nameUser} - Posted on ${dataComment.date}`
    const textCommentData = createElement("p", "item-text-data", commentData);
    itemText.append(textCommentData);
    divComment.append(itemText);
   //final append
    divReviews.append(divComment);
    return divReviews
}


function createBalls(property, divStars){
    const maxBall = 5;    
    for(let i = 1; i <= property; i++){
        const imgStarYellow = createElement("img", "star-yellow");
        imgStarYellow.src = "../image/star-yellow.svg";
        imgStarYellow.alt = "star-yellow";
        divStars.append(imgStarYellow);
    }
    if(property < maxBall){
        for(let i = property + 1; i <= maxBall ; i++){ 
            const imgStarGray = createElement("img", "star-gray");
            imgStarGray.src = "../image/star-gray.svg";
            imgStarGray.alt = "star-yellow";
            divStars.append(imgStarGray);
        } 
    }
    
}
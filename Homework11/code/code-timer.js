/*Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. 
Використовуючи JS виведіть у консоль відступ
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/
/*const div = document.createElement("div");
document.body.prepend(div); 
console.log(getComputedStyle(div).margin);*/

function clearClass () {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}

function showInterval (){
    secCount += 1;     
  
    if(secCount === 60){
        minCount += 1 ;
        secCount = 0;
    }

    if(minCount === 60){
        hourCount += 1;
        minCount = 0;
    }     
    secCount > 9 ? sec.innerText = secCount : sec.innerText = `0${secCount}`;
    minCount > 9 ? min.innerText = minCount : min.innerText = `0${minCount}`;
    hourCount > 9 ? hour.innerText = hourCount : hour.innerText = `0${hourCount}`;
}

const stopwatch = document.querySelector(".container-stopwatch"),
btnStart = document.getElementById("start"), 
btnStop = document.getElementById("stop"), 
btnReset = document.getElementById("reset"),
sec = document.getElementById("sec"),
min = document.getElementById("min"),
hour = document.getElementById("hour");

let isClick = false; // кнопка Start ще не була натиснута 
let secCount = 0, minCount = 0, hourCount = 0;
let timerId = 0;

btnStart.onclick = () =>{
    if(!isClick){
        isClick = true;
        clearClass();
        stopwatch.classList.add("green"); 
        timerId = setInterval(() => {
            showInterval();
        }, 1000);
    }; 
};

btnStop.onclick = () =>{
    clearInterval (timerId);
    isClick = false;
    clearClass();
    stopwatch.classList.add("red");
};

btnReset.onclick = () =>{
    clearInterval (timerId);
    // timerId = 0;
    isClick = false;
    clearClass();
    stopwatch.classList.add("silver")
    sec.innerText = `00`
    min.innerText = `00`
    hour.innerText = `00`; 
    secCount = 0, minCount = 0, hourCount = 0;
}





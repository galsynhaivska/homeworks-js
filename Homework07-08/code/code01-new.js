/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага,
водій типу Driver, мотор типу Engine.
Методи start(), stop(), turnRight(), turnLeft(),
які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.
Створити похідний від Car клас - Lorry (вантажівка), 
що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, 
який також характеризується граничною швидкістю.
*/

class Driver {
    constructor(lastName, firstName, middleName, experience){
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.experience = experience;
    }
}

class Engine extends Driver{
    constructor(motorPower, producer, lastName, firstName, middleName, experience){
        super(lastName, firstName, middleName, experience);
        this.motorPower = motorPower;
        this.producer = producer;
    }
}

class Car extends Engine{
    constructor(motorPower, producer,lastName, firstName, middleName, experience, 
                carBrand, carClass, carWeight){
        super(motorPower, producer, lastName, firstName, middleName, experience);
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.carWeight = carWeight;
    }   
        start() { 
            return "Поїхали";
        }
        stop() {
            return "Зупиняємося";
        }    
        turnRight() {
            return "Поворот праворуч";
        }
        turnLeft() {
            return "Поворот ліворуч";
        }
        toString() {
            return `<br> 
            Інформація про автомобіль: <br>
            марка автомобіля ${this.carBrand} <br>
            клас автомобіля ${this.carClass}<br> 
            вага ${this.carWeight}<br>
            мотор ${this.motorPower}, виробник ${this.producer} <br>
            водій ${this.lastName} ${this.firstName} ${this.middleName} <br>
            стаж водіння ${this.experience} <br> `
        }
}

class SportCar extends Car{
    constructor(motorPower, producer,lastName, firstName, middleName, experience, carBrand, carClass, carWeight, maxSpeed){
        super(motorPower, producer,lastName, firstName, middleName, experience, carBrand, carClass, carWeight);
        this.maxSpeed = maxSpeed;
    
}
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass} виробник ${this.producer} <br>
        водій ${this.lastName} ${this.firstName} ${this.middleName} <br>
        стаж водіння ${this.experience} <br> 
        гранична швидкість ${this.maxSpeed} <br>`
    }
}
class Lorry extends Car {
    constructor(motorPower, producer,lastName, firstName, middleName, experience, carBrand, carClass, carWeight, capacity){
        super(motorPower, producer,lastName, firstName, middleName, experience, carBrand, carClass, carWeight);
        this.capacity = capacity;
    }
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass}<br> 
        вага ${this.carWeight}<br>
        мотор ${this.motorPower}, виробник ${this.producer} <br>
        водій ${this.lastName} ${this.firstName} ${this.middleName} <br>
        стаж водіння ${this.experience} <br> 
        вантажопідйомність ${this.capacity} <br>`
    }    
}

function CarToString(arrCar) {
    for (let i=0; i < arrCar.length; i++){
        arrCar[i].toString()
    }
}


const drivers = [new Driver("Ivaniv", "Petro", "Marianovych", 7), 
                    new Driver("Petriv", "Mark", "Serhijovych", 2),
                    new Driver("Volyk", "Victor", "Petrovych", 12)];

const engines = [new Engine("135 kW", "BMW", "Volyk", "Victor", "Petrovych", 12), 
                new Engine("150 kW", "Audi",  "Petriv", "Mark", "Serhijovych", 2),
                new Engine("115 kW", "Ford",  "Volyk", "Victor", "Petrovych", 12),
                new Engine("120 kW", "Opel", "Ivaniv", "Petro", "Marianovych", 7),
                new Engine("331 kW", "Porsche", "Petriv", "Mark", "Serhijovych", 2), 
                new Engine("103kW", "Hyundai", "Volyk", "Victor", "Petrovych", 12)];

const cars = [new Car("135 kW", "BMW", "Volyk", "Victor", "Petrovych", 12, "BMW G20", "D", "1800"),
            new Car("150 kW", "Audi",  "Petriv", "Mark", "Serhijovych", 2,  "Audi TT", "S", "1500"), 
            new Car("115 kW", "Ford",  "Volyk", "Victor", "Petrovych", 12,  "Ford Fiesta", "B", "1150"),
            new Car("120 kW", "Opel", "Ivaniv", "Petro", "Marianovych", 7,  "Opel Astra", "C", "1400")]; 

const sportCars = new SportCar("331 kW", "Porsche", "Petriv", "Mark", "Serhijovych", 2,
                            "Porsche 911", "S", "1500", "306 km/h");
const lorry = new Lorry("103kW", "Hyundai", "Volyk", "Victor", "Petrovych", 12, "Hyundai", "S", "2500", "4600 kg");

const arrCar = [cars, sportCars, lorry];

console.log(drivers);
console.log(engines);
console.log(cars);

console.log(sportCars);
console.log(lorry);

console.log(arrCar);

console.log(`${cars[0].carBrand} : ${cars[0].turnLeft()}`);
console.log(`${cars[1].carBrand} : ${cars[0].start()}`);
console.log(`${cars[2].carBrand} : ${cars[0].turnRight()}`);
console.log(`${cars[3].carBrand} : ${cars[0].stop()}`);
console.log(`${cars[0].lastName} : ${cars[0].stop()}`);
document.write(cars.join(" ").toString(), sportCars.toString(), lorry.toString());

const spans = [];
const display = document.querySelector(".display");
// for Heroes of Stars
let counter = 1;
let nextStep = false;
let tableHeroes = "";
let nextLoad = "";
const btnLoad = document.createElement("button");
btnLoad.innerText = "Завантажити ще...";
btnLoad.classList.add("btn-load");

async function reqKurs (url) {
    document.querySelector(".loader").classList.add("active");
    const data = await fetch(url);
    return data.json()
}

async function reqStars (url) {
    document.querySelector(".loader").classList.add("active");
    const data = await fetch(url);
    return data.json()
}

async function req (url) {
    document.querySelector(".loader").classList.add("active");
    const data = await fetch(url);
    return data.json()
}

function showStars(data){
    display.innerHTML = "";
    if(nextStep === false){     //перше завантаження
        nextStep = true;
        tableHeroes = `
        <table>
            <thead>
                <tr>
                    <th>N</th>
                    <th>Ім'я</th>
                    <th>Рік</th>
                    <th>Стать</th>
                    <th>Зріст</th>
                    <th>Вага</th>
                </tr>
            </thead>
            <tbody>`
    }
    nextLoad = data.next;
        for (let i = 0; i < data.results.length; i++){
            tableHeroes += `     
                    <tr>
                    <td>${i+counter}</td>
                    <td>${data.results[i].name}</td>
                    <td>${data.results[i].birth_year}</td>
                    <td>${data.results[i].gender}</td>
                    <td>${data.results[i].height}</td>
                    <td>${data.results[i].mass}</td>
                    </tr> `
        }

    counter += data.results.length; 
    document.querySelector(".loader").classList.remove("active");    
    display.insertAdjacentHTML("beforeend", tableHeroes);
    display.append(btnLoad);
    btnLoad.addEventListener("click", loadElseReq);
    
}

const loadElseReq = () =>{
    if(nextLoad !== null){
        reqStars(nextLoad)
            .then(info => showStars(info));
    }    
    document.querySelector(".loader").classList.remove("active");
}


function showKurs(data) {
    display.innerHTML = "";
    const today = new Date();
    const month = today.getMonth() + 1 < 10 ? `0${today.getMonth() + 1}` : today.getMonth() + 1; 
    const h2 = `
    <h2>Офіційний курс гривні на ${today.getDate()}.${month}.${today.getFullYear()}
    </h2>
    <table>
        <thead>
            <tr>
                <th>N</th>  
                <th>Код валюти</th>
                <th>Назва валюти</th>
                <th>Літерний код</th>
                <th>Офіційний курс</th>
            </tr>
        </thead>
        <tbody>
        ${data.map((obj, i) => {
            return `
                 <tr>
                     <td>
                     ${i}
                     </td>
                     <td>
                     ${obj.r030}
                     </td>
                     <td>
                     ${obj.txt}
                     </td>
                     <td>
                     ${obj.cc}
                     </td>
                     <td>
                     ${obj.rate}
                     </td>
                 </tr>`
        }).join("")}    

        </tbody>
    </table>
    `
    display.insertAdjacentHTML("beforeend", h2);
    document.querySelector(".loader").classList.remove("active");
 
}

function show(data) {
    display.innerHTML = "";
    const table = `
        <table>
         <thead>
          <tr>
           <th> console.log(info)
            N 
           </th>
           <th>
            Задача
           </th>
           <th>
            Статус
           </th>
           <th>
            Редагувати
           </th>
          </tr>
         </thead>
         <tbody>
             ${data.map((obj, i) => {
                const span = document.createElement("span");
                span.addEventListener("click", () => {
                    clickHendler(obj)
                });
                span.innerHTML = "&#128295;";
                spans.push(span);
                 return `
                     <tr>
                         <td>
                         ${obj.id}
                         </td>
                         <td>
                         ${obj.title}
                         </td>
                         <td>
                         ${obj.completed ? "&#9989" : "&#10060"}
                         </td>
                         <td data-span="${i}">
                         </td>
                     </tr>`
            }).join("")}
           </tbody>
        </table>    
       `
    display.insertAdjacentHTML("beforeend", table);
    
    document.querySelectorAll("td[data-span]").forEach((e, i) => {
        console.log();
        e.append(spans[i])
    })
    document.querySelector(".loader").classList.remove("active");
}

function clickHendler(obj){
    document.querySelector(".parent").classList.add("active");
    document.getElementById("description").innerText = obj.title;
    document.getElementById("status").checked = obj.completed;
   
    document.getElementById("save").onclick = () => {
        console.log("+");
        const l = JSON.parse(localStorage.history);
        console.log(l);
        console.log(obj);
        l.push(obj);
        localStorage.history = JSON.stringify(l);  
        document.querySelector(".parent").classList.remove("active");   
    }
}


export { req, show, reqKurs, showKurs, showStars, reqStars};

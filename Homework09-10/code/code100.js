/* При загрузке страницы - показать на ней кнопку с текстом “Нарисовать круг”.
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью JavaScript.
 - При нажатии на кнопку “Нарисовать круг” показывать одно поле ввода - диаметр круга. 
 - При нажатии на кнопку “Нарисовать” создать на странице 100 кругов (10 * 10) случайного цвета.
 - При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
то есть все остальные круги сдвигаются влево.
*/
//функція для створення елементів input і  button "Намалювати"
function createElement(nameEl, classEl, typeEl){
    const elem = document.createElement(nameEl);
    if(classEl !== "" || classEl !== null){
        elem.className = classEl;
    }
    if(typeEl !== "" || typeEl !== null){
        elem.type = typeEl;
    }
    return elem;
}
//функція видаляє значення, яке користувач вводив раніше, відновлює placeholder і
//встановлює фокус на полі вводу
function clearInput(obj){
    obj.value ="";
    obj.placeholder = "Задайте діаметр круга: число від 10 до 200";
    obj.focus();
}
//функція для створення div-кругів
function createDiv(nameEl, i) {
    const elem = document.createElement(nameEl);
    elem.className = 'd' + i;
    elem.style.backgroundColor = `hsl(${Math.floor(Math.random()*360)}, 100%, 50%)`;
    elem.style.width = `${parseFloat(inputDraw.value)}px`;
    elem.style.height = `${parseFloat(inputDraw.value)}px`;
    elem.style.borderRadius = '50%';
    return elem;
}

//створюємо елементи input, button "Намалювати" i div-контейнер, в якому будуть розміщені круги
let inputDraw = createElement("input","input-text", "text"),
    btnDraw = createElement("button", "btn-draw", "button"),
    divs = createElement("div", "div-container", "");

// визначаємо назву кнопки 
btnDraw.textContent = "Намалювати";
btnStart = document.getElementById("start");

let a = 0; // змінна-лічильник; візначає кількість кліків по кругах

//подія - клік по кнопці "Намалювати круг"
btnStart.onclick = () => {
   btnStart.remove();               //видаляємо кнопку  "Намалювати круг"
   document.body.append(inputDraw); //додаємо input
   document.body.append(btnDraw);   //додаємо кнопку  "Намалювати"
   clearInput(inputDraw);           //встановлюємо фокус на полі вводу
}

//подія - клік по кнопці "Намалювати"
btnDraw.onclick = () =>{
// спочатку перевіряємо, що ввів користувач   
    if(parseFloat(inputDraw.value) - inputDraw.value !== 0 ){
        alert("Це не число! Спробуйте ще раз!");
        clearInput(inputDraw);
    }
    else if(parseFloat(inputDraw.value) < 10 || parseFloat(inputDraw.value) > 200){
        alert("Введіть число від 10 до 200!");
        clearInput(inputDraw);
    }
    //якщо все правильно, малюємо div-кола
    else{
        inputDraw.remove();           //видаляємо input"        
        btnDraw.remove();            //видаляємо кнопку  "Намалювати"
        document.body.prepend(divs); //додаємо div-контейнер
    // додаємо div-кола у div-контейнер    
        for (i = 1; i <= 100; ++i) {
            div = createDiv("div", i);
            divs.append(div); 
        }
    //отримуємо колекцію  div-кругів
        let [...cycle] = document.querySelectorAll("div>div");

        cycle.forEach(function(item) {
            item.onclick = () => {     // подія - клік по  колу з колекції
                item.remove();         // видаляємо коло
                a +=  1;               // рахуємо скільки видалено
            //якщо видалено всі кругів видаляємо div-контейнер
            //і додаємо кнопку  "Намалювати круг"
                if(a == 100) {         
                    divs.remove()
                    document.body.prepend(btnStart);
                }  
            }
        })
    }
}
               





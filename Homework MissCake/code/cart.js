import {cakes} from "../code/data.js";
import {clickShoppingBag, blockClass} from "./function.js";


//MyBag click - no reaction
document.querySelector(".shopping-bag").addEventListener("click", clickShoppingBag);
//order 
let order;
if(!window.localStorage.order){
    order = [];
}else{
    order = JSON.parse(window.localStorage.order);
}    
const orderBag = document.querySelector(".counter-shopping-bag");
//orderBag.innerText = order.length;   // якщо рахуємо кількість видів кейків
orderBag.innerText = `${ order.reduce((total, item) => {
    total += parseInt(item.quantity);
    return total;
}, 0)}`;      // якщо рахуємо кількість кейків


document.querySelector(".shopping-bag").classList.add("blocked");

// fill cart
const cartContainer =document.querySelector(".cart-container");
cartContainer.innerHTML = "";
//const screenWidth = window.screen.width;
const screenWidth = window.innerWidth;
//console.log(screenWidth)
cartContainer.insertAdjacentHTML("beforeend", order.map((item) => {
    if(screenWidth  < 768){
        cartContainer.classList.add("mobile");
        cartContainer.classList.remove("container");
        return `
            <div class="item-cart">
                <div class="side-top">
                    <div class="item-cart-img">
                        <img src="../image/${item.image}" alt=${item.cakeName}>   
                    </div>            
                        <div class="item-cart-info">
                            <h3 class="item-title-cart">${item.cakeName}</h3>
                            <div class="line-cart-item"></div>
                        </div>                  
                </div>        
                 <div class="side-bottom">
                    <div class="count-cart-btn">
                        <div class="counter-cart">
                            <button class="minus" type="button"> - </button>
                            <div class="result result-cart"> ${item.quantity} </div>
                            <button class="plus" type="button"> + </button>
                        </div>
                    </div>
                    <div class="price-item-cart">$${(item.price * item.quantity).toFixed(2)}</div>
                </div>
            </div>
        `
    }else{
    return `
        <div class="item-cart">
            <div class="item-cart-img">
                <img src="../image/${item.image}" alt=${item.cakeName}>   
            </div>
            <h3 class="item-title-cart">${item.cakeName}</h3>
            <div class="count-cart-btn">
                <div class="counter-cart">
                    <button class="minus" type="button"> - </button>
                    <div class="result result-cart"> ${item.quantity} </div>
                    <button class="plus" type="button"> + </button>
                </div>
            </div>
            <div class="price-item-cart">$${(item.price * item.quantity).toFixed(2)}</div>
        </div>
    ` 
    }
}).join("")); 
const totalDiv = document.querySelector(".total-cart .price-item-cart");
if(screenWidth  < 768){
    totalDiv.classList.add("total-price");
    totalDiv.innerText = "";
    totalDiv.innerText = `$${ order.reduce((total, item) => {
        total += item.price * item.quantity;
        return total
    }, 0).toFixed(2)}`;
    
}else{
        totalDiv.innerText = "";
        totalDiv.innerText = `$${ order.reduce((total, item) => {
            total += item.price * item.quantity;
            return total
        }, 0).toFixed(2)}`;    
}

const changeItemCart = (ev) =>{
    ev.preventDefault();
    const itemCart = document.querySelectorAll(".item-cart")
  //  console.log(itemCart);
    itemCart.forEach(el =>{
        if(ev.target.parentElement.parentElement === el.querySelector("div.count-cart-btn")){
            const divItemCart = el.querySelector(".item-title-cart");
            const divResult = el.querySelector(".result.result-cart");
            const divPrice = el.querySelector(".price-item-cart");
        //    console.log(ev.target, divItemCart, divResult, divPrice, totalDiv);    
        //    console.log(divResult.offsetTop, divResult.offsetLeft)
            if(ev.target.className === "plus"){  
                divResult.innerText = parseInt(divResult.innerText) + 1;
            }

            if(ev.target.className === "minus"){    
                if(parseInt(divResult.innerText) !== 0){
                    divResult.innerText = parseInt(divResult.innerText) - 1;
                    if(parseInt(divResult.innerText) === 0){
                        const divModalCart = document.querySelector(".div-modal-cart");
                    //    console.dir(divResult)
                        divModalCart.style.top = divResult.offsetTop + 25 + "px"; 
                        divModalCart.style.left = divResult.offsetLeft - 100 + "px"; 
                        divModalCart.style.display = "block";
                    //блокуємо кнопки + -    
                        const allBtn = document.querySelectorAll("button");
                        const allRef =  document.querySelectorAll("a");
                        
                        blockClass(allRef, "add");
                        blockClass(allBtn, "add");
                        const btnYes = document.querySelector(".yes-btn");
                        const btnNo = document.querySelector(".no-btn");
                        btnNo.addEventListener("click", () => {
                            divModalCart.style.display = "none";
                        //відміна блокування "+""-"
                            blockClass(allRef, "remove");
                            blockClass(allBtn, "remove"); 
                        });
                        btnYes.addEventListener("click", () =>{
                            order.forEach((el, i) => {
                                if(el.cakeName === divItemCart.innerText){
                                //    console.log(el.cakeName, i)
                                    order.splice(i, 1);
                                }   
                            });
                            window.localStorage.order = JSON.stringify(order);

                            divModalCart.style.display = "none";
                //            console.log(divItemCart, el)
                //            console.log(divItemCart, el.querySelector(".item-cart"))
                            el.remove();
                           
                            //відміна блокування "+""-"
                            blockClass(allRef, "remove");
                            blockClass(allBtn, "remove");                 
                        });
                    }  
                }
            }
            order.forEach(el => {
                if(el.cakeName === divItemCart.innerText){
                    el.quantity = divResult.innerText;
            //        console.log(el.quantity * el.price)
                    divPrice.innerText = `$${(el.quantity * el.price).toFixed(2)}`
                }
            });
            window.localStorage.order = JSON.stringify(order);
            totalDiv.innerText = `$${ order.reduce((total, item) => {
                total += item.price * item.quantity;
                return total
            }, 0).toFixed(2)}`;
             //orderBag.innerText = order.length;  
             orderBag.innerText = `${ order.reduce((total, item) => {
                total += parseInt(item.quantity);
                return total;
            }, 0)}`; 
        }
    });
}



//click on '+' or '-'
const plus = document.querySelectorAll(".plus");
const minus = document.querySelectorAll(".minus");
//console.log(document.querySelectorAll(".plus"));
plus.forEach(item => {
    item.addEventListener("click", changeItemCart);
});    
minus.forEach(item => {
    item.addEventListener("click", changeItemCart);
});    



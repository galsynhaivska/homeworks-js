//функція-конструктор класу Human
function Human(name, name2, lastname, age, city){
    this.name = name;
    this.name2 = name2;
    this.lastname = lastname;
    this.age = age;
    this.city = city;

//властивість функції-конструктора Human - лічильник її викликів (статична властивість)    
    Human.getCount++;
}

//метод show для екземплярів класу Human
Human.prototype.show = function() {
    return `Картка користувача ${this.name} <br/>
    ПІБ: ${this.lastname} ${this.name}  ${this.name2} <br>
    Вік: ${this.age} <br>
    Місто: ${this.city}<br><br>`;
}
//функція для сортування по age
function sortAge(array){
    array.sort((a, b) => a.age > b.age ? 1: -1);
}
//функція для сортування по name
function sortName(array){
    array.sort((a, b) => a.name.localeCompare(b.name));
}

Human.getCount = 0;

//створюємо обєкти і виводимо їх на екран
const user1 = new Human("Іван", "Петрович", "Сніжко", 10, "Київ");
//let user1 = userArr.push(new Human("Іван", "Петрович", "Сніжко", 10, "Київ"));
const user2 = new Human("Павло", "Семенович", "Корнійчук", 56, "Львів");
const user3 = new Human("Андрій", "Йосипович", "Михалюк", 21, "Луцьк");
const user4 = new Human("Олег", "Олександрович", "Дубко", 5, "Рівне");
const user5 = new Human("Марія", "Олександрівна", "Гончаренко", 19, "Львів");
const user6 = new Human("Вікторія", "Михайлівна", "Дзюба", 33, "Одеса");
document.write(user1.show());
document.write(user2.show());
document.write(user3.show());
document.write(user4.show());
document.write(user5.show());
document.write(user6.show());
document.write(`Всього ${Human.getCount} користувачів <br>`);

//створюємо мвсив з об'єктів
let userArr = [user1, user2, user3, user4, user5, user6];


document.write(`<br> Результат сортування по віку: <br>`);
sortAge(userArr);
for(let i = 0; i <= userArr.length - 1; i++){
    document.write(`${userArr[i].age}, ${userArr[i].name} <br>`);
}
document.write(`<br>Результат сортування по іменам: <br>`);
sortName(userArr);
for(let i = 0; i <= userArr.length - 1; i++){
    document.write(`${userArr[i].name}, ${userArr[i].age} <br>`);
}
    




/* Создайте поле для ввода данных,
 поcле введения данных, выведите текст под полем 
 */
 window.addEventListener("DOMContentLoaded", () => {
    const inputDiv = document.createElement("input"),
        labelDiv = document.createElement("label"),
        divTask08 = document.createElement("div"),
        divTask07 = document.querySelector(".login-input");

    divTask08.classList.add("div-task08");

    inputDiv.type = "text";
    labelDiv.innerText = "Введіть текст: ";

    divTask07.after(divTask08);
    divTask08.append(labelDiv);
    divTask08.append(inputDiv);

    inputDiv.addEventListener("change", () =>{
        if(document.querySelector(".p-task08") === null){
            const p = document.createElement("p");
            p.classList.add("p-task08");
            inputDiv.after(p);
            console.log(document.querySelector(".p-task08"));
            p.innerText = `Ви ввели:  ${inputDiv.value}`;
        }else{
            p = document.querySelector(".p-task08");
            p.innerText = `Ви ввели:  ${inputDiv.value}`;
        }
    });
});




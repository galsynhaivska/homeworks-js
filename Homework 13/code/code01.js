/* 1. Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут 
виводить своє value в параграф з id="test"*/
//import from "./task1.js"
window.addEventListener("DOMContentLoaded", () =>{
    const [...input] = document.querySelectorAll("input");
     input.forEach(element => {
        element.addEventListener("change", change)  // or "blur"
    });
});

function change(e = window.event){
    document.querySelector("#test").innerText = e.target.value;
}
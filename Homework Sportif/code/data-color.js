export const colors = [
    {colorName: 'navi', hex: '#000080', rgba: '0, 0, 128'},
    {colorName: 'blue', hex: '#0000FF', rgba: '0, 0, 255'},
    {colorName: 'lightblue', hex: '#B6DDF0', rgba: '173, 216, 230'},
    {colorName: 'khaki', hex: '#F0E68C', rgba: '233, 212, 189'},
    {colorName: 'desert', hex: '#D2B48C', rgba: '210, 180, 140'},
    {colorName: 'black', hex: '#000000', rgba: '0, 0, 0'},
    {colorName: 'olive', hex: '#808000', rgba: '128, 128, 0'},
    {colorName: 'pewter', hex: '#979595', rgba: '140, 130, 130'},
    {colorName: 'green', hex: '#008000', rgba: '0, 128, 0'},
    {colorName: 'white', hex: '#FFFFFF', rgba: '255, 255, 255'},
    {colorName: 'natural', hex: '#FAF0E6', rgba: '250, 240, 230'},
    {colorName: 'gray', hex: '#808080', rgba: '128, 128, 128'},
    {colorName: 'red', hex: '#800000', rgba: '255, 0, 0'},
    {colorName: 'sand', hex: '#f5edc8', rgba: '245, 217, 192'},
    {colorName: 'chambray', hex: '#797c85', rgba: '51, 50, 50'},
    {colorName: 'charcoal', hex: '#444343', rgba: '107, 113, 124'},
];
export const brands = [];
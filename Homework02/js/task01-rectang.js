const widthFigure = parseInt(prompt("Введіть довжину прямокутника \n (ціле число від 3 до 20)", 7));
const heightFigure = parseInt(prompt("Введіть ширину прямокутника \n (ціле число від 3 до 20)", 5));

if ( (widthFigure >= 3  && widthFigure <= 20) && (heightFigure >= 3  && heightFigure <= 20) ) {
 // Rectangular
    for(let i = 1; i <= heightFigure; i++){
        if(i == 1 ){
            for(let k = 1; k <= widthFigure; k++){
                document.write("*");
            }
            document.write("<br>");   
            continue;
        }
        if(i == heightFigure){
            for( k = 1; k <= widthFigure; k++){
                document.write("*");
            }
            document.write("<br>"); 
            break;  
        }
        for(let j = 1; j <= widthFigure; j++){
            if(j == 1 || j == widthFigure){
                document.write("*");   
            } else{
                document.write("&nbsp;&nbsp;"); 
            }        
        }    
        document.write("<br>");
    }
    
//if input wrong number        
} else if ((widthFigure < 2  || widthFigure > 20) || (heightFigure < 3  || heightFigure > 20)) {
    alert("Ви неправильно вказали довжину чи ширину!");
//if input any other symbols or click Cancel           
} else {
    alert("Помилка вводу даних. Спробуйте ще раз");
}
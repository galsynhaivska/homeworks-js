const itemDescription = [
    {
      idItem: "670170",
      reviewCounter: "25",
      descripton: `With its classic design, practical features and unequaled comfort - it's no wonder Sportif 'Original' stretch cargo shorts are practically the uniform on yachts worldwide! A customer favorite for over 50 years, they're called the world's best boating shorts. Featuring a 4" inseam for sizes 30-40 and 5.5" inseam for sizes 42-46.`,
      details: [
      "Constructed from our 7.5 oz. Original Stretch Twill fabric", 
      "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets,with button-through flap closure, and two zippered security back pockets",
      "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
      "Non-roll stretch waistband",
      "Sturdy tunnel belt loops",
      "Durable double fabric seat",
      "Gusset crotch",
      "Double-stitched seams throughout",
      'Bar tack stitching reinforces areas of stress to guarantee years of service 4" inseam (sizes 30-40) or 5.5" inseam (sizes 42-46)',
      "Wrinkle resistant 62% polyester, 35% cotton, 3% spandex blend is travel friendly and ready to wear straight from the dryer",
      "Machine wash",
      "Imported"
    ]  
    },
    {
      idItem: "0M019910",
      reviewCounter: "63",
      descripton: `The same 100% rugged Cotton Camp Cloth as the Island Shorts by Hook & Tackle but in an 8" inseam cargo style short. Features comfort side elastic  waistband with handy D-ring, and pockets galore: deep expandable front pockets, front cargo flap pockets, beverage can/phone pocket, hidden security pocket and secure zip back pockets.`,
      details: [ 
        "Comfort side elastic waistband", 
        "Deep expandable front pockets", 
        "Front cargo flap pockets",
        "Beverage can/utility/cell phone pocket",
        "Secure zip back pocket",
        "Handy D-ring",
        `8" inseam`,
        "100% Cotton",
        "Machine wash",
        "Imported" 
        ] 
    },
    {
        idItem: "0M039850",
        reviewCounter: "1",
        descripton: `Be your own guide to comfort, when you slip into this 100% cotton-rich canvas drawstring short. Guaranteed to have a weekend experience,  no matter the day of the week. Whether cruising, touring, boating, or just out  for a casual day in the city, this deck short will fit the bill.`,
        details: ["Comfortable back elastic waistband",
            "Drawcord for adjustability",
            "Deep front pockets",
            "Back patch pockets, left side has a button-thru closure",
            `6" inseam`,
            "100% Cotton rugged mariner cloth",
            "Machine wash",
            "Imported"
        ], 
      },
      {
        idItem: "B0574760DT",
        reviewCounter: "2",
        descripton: `The Go To Short is a unique choice for the active adventurer. This versatile short is sustainable, made from a blend of hemp and recycled polyester. Combined, these fibers help reduce water and fossil fuel usage and have a significantly lighter footprint on the earth. Easy on the conscience, but easier still on even the most demanding summer days. Hemp and polyester  are strong and durable, yet lightweight and comfortable enough to wear all day long. Paired with just 3% of spandex, this pull-on short features the  perfect amount of stretch to keep up with you.`,
        details: ["Pull-on styling",
            "Full elastic drawstring waist",
            `10" inseam`,
            "Stretch drawcord",
            "Two slash pockets",
            "Single back pocket",
            "Sustainable style",
            "53% hemp, 44% recycled polyester, 3% spandex",
            "Machine wash cold, tumble dry, cool iron",
            "Designed in the USA",
            "Made in a Fair Trade Certified™ factory in India",
        ], 
      },
      {
        idItem: "674170",
        reviewCounter: "6",
        descripton: `These classic nautical cargo pants are built to last and designed 
            to exceed your expectations. Crafted from our stretch twill blend that offers 
            the comfort of cotton, the wrinkle resistance of polyester and the mobility of
             spandex.`,
        details:[
          "Constructed from our 7.5 oz. Mid-Weight Stretch Twill fabric",
        "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets",
        "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
        "Non-roll stretch waistband",
        "Sturdy tunnel belt loops",
        `37" unfinished inseam is ready to be hemmed to your exact measurements`,
        "62% polyester, 35% cotton, 3% spandex blend is travel-friendly and ready to wear straight from the dryer",
        "Machine wash",
        "Imported"
        ], 
      },
      {
        idItem: "0M019100",
        reviewCounter: "26",
        descripton: `The same rugged Cotton Camp Cloth as the Island Shorts by Hook & Tackle but in 33" finished inseam pants. Features comfort back elastic waistband, and pockets galore: deep front pockets, beverage can/phone pocket, hidden security pocket and secure zip back pocket.`,
        details:[
          "Comfort back elastic waistband",
        "Salt-washed for broken-in comfort",
        "Deep expandable front pockets",
        "Zipper secured back pocket",
        "Hidden security pocket",
        "Beverage can/utility/cell phone pocket",
        `37" finished inseam`,
        "100% Cotton",
        "Machine wash",
        "Imported"
        ], 
      },
    {
      idItem: "H5005200DT",
      reviewCounter: "26",
      descripton: `Known for bringing you comfort, Ecoths delivers again with the Chambers PJ Pant. These pants are made from 100% cotton, creating a soft-yet-tough flannel that will help regulate your temperature at night, while delivering hypoallergenic comfort. These classic pull-on PJs are secured with an elastic waist and drawcord and feature Buffalo check print reminiscent of winter trips to the cabin. It's time to get cozy!`,
      details:[
        "Classic pull on pajama pant",
        "Elastic waist with drawcord",
        "Checkered print fabric",
        "Machine wash cold, tumble dry low, warm iron",
        `31" inseam`,
        "100% Cotton",
        "Designed in the USA",
        "Made in a Fair Trade Certified™ factory in India"
      ], 
  },
  {
    idItem: "482162",
    reviewCounter: "20",
    descripton: `The Calcutta chinos are crafted from a lightweight tropical stretch fabric to keep you cool and comfortable in the hottest of climates.`,
    details:[
      "Constructed from our 5.0 oz. lightweight Tropical Twill stretch fabric",
      "4-pocket design includes two deep front pockets, two back pockets with button-through flap closure. A small zip section in the right front pocket is great for travel",
      "Non-roll stretch waistband",
      "Narrow belt loops",
      `37" unfinished inseam is ready to be hemmed to your exact measurements`,
      "Wrinkle resistant 60% polyester, 37% cotton, 3% spandex",
      "Machine wash",
      "Imported"
    ], 
},
{
  idItem: "00TRMM90",
  reviewCounter: "4",
  descripton: `Known for bringing you comfort, Ecoths delivers again with the Chambers PJ Pant. These pants are made from 100% cotton, creating a soft-yet-tough flannel that will help regulate your temperature at night, while delivering hypoallergenic comfort. These classic pull-on PJs are secured with an elastic waist and drawcord and feature Buffalo check print reminiscent of winter trips to the cabin. It's time to get cozy!`,
  details:[
    "Classic pull on pajama pant",
    "Elastic waist with drawcord",
    "Checkered print fabric",
    "100% Cotton flannel",
    `31" inseam`,
    "Machine wash cold, tumble dry low, warm iron",
    "Designed in the USA",
    "Made in a Fair Trade Certified™ factory in India"
  ], 
},
{
  idItem: "B0575300DT",
  reviewCounter: "10",
  descripton: `The casual Hybrid Short is great for the active adventurer. This versatile short is made from 45% recycled polyester, which not only causes less harm to the planet but also saves plastic from being thrown into a landfill. Easy on the conscience, but easier still on even the most demanding summer days. Polyester is strong and durable, not to mention wrinkle-resistant, yet lightweight and comfortable enough to be worn all day long. With just enough stretch to keep up with you, this pull-on short is sure to become your go-to this summer.`,
  details:[
    "Pull-on styling",
    "Full elastic drawstring waist",
    '10" inseam',
    "Stretch drawcord",
    "Two slash pockets",
    "Single back pocket",
    "Sustainable style",
    "45% recycled polyester, 45% polyester, 10% spandex",
    "Machine wash cold, tumble dry, cool iron",
    "Designed in the USA",
    "Made in a Fair Trade Certified™ factory in India",

  ], 
},
{
  idItem: "618170",
  reviewCounter: "36",
  descripton: `These shorts are built for action and guaranteed to last for years and years! They have all the great features of our best-selling 'Original' Stretch Cargo Shorts in a longer 7.5" inseam.`,
  details:[
    "Constructed from our 7.5 oz. Original Stretch Twill fabric",
    "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets",
    '7.5" inseam',
    "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
    "Non-roll stretch waistband",
    "Sturdy tunnel belt loops",
    "Bar tack stitching reinforces areas of stress to guarantee years of service",
    "45% recycled polyester, 45% polyester, 10% spandex",
    "Machine wash cold, tumble dry, cool iron",
    "Designed in the USA",
    "Imported",
  ], 
},
{
  idItem: "610162",
  reviewCounter: "52",
  descripton: `The Lauderdale Stretch Cargo Shorts have the same great design as our bestselling Sportif Original Cargo Shorts, but in a lighter tropical weight stretch twill fabric that will keep you cool and comfortable in the hottest of climates. Featuring a 4" inseam for sizes 30-40 and 5.5" inseam for sizes 42-46.`,
  details:[
    "Constructed from our 5.0 oz. lightweight Tropical Stretch Twill fabric",
    "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets",
    '4" inseam (sizes 30-40) or 5.5" inseam (sizes 42-46)',
    "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
    "Non-roll stretch waistband",
    "Gusset crotch",
    "Sturdy tunnel belt loops",
    "Bar tack stitching reinforces areas of stress to guarantee years of service",
    "45% recycled polyester, 45% polyester, 10% spandex",
    "Machine wash",
    "Imported",
  ], 
},
{
  idItem: "0M039810",
  reviewCounter: "15",
  descripton: `You don't have to "trade" looks for comfort. Constructed from a sturdy twill with a back elastic waistband, these 6" inseam shorts are built with durability and comfort in mind.`,
  details:[
    "Deep front slash pockets",
    "Front hook and loop cargo pockets",
    "Secure hook and loop back pockets",
    "Comfortable back elastic waistband",
    "Reinforced stress points",
    "Tunnel belt loops",
    '6" inseam',
    "60% cotton, 40% polyester, color Chambray 100% Cotton",
    "Machine wash",
    "Imported",
  ], 
},
{
  idItem: "05581212",
  reviewCounter: "18",
  descripton: `Whether you're on deck or around town, you'll appreciate these easy to wear shorts with an elastic waistband and hidden internal drawstring for an adjustable fit. The long-wearing 100% cotton twill will keep you comfortable and the 8.5" inseam will keep you covered. Deep front pockets for keys and a back patch pocket on the right side for your wallet.`,
  details:[
    "Pull-on styling with full elastic waistband",
    "Hidden internal drawstring",
    "Deep front pockets",
    '8.5" inseam',
    "Long-wearing 100% cotton twill",
    "Machine wash and tumble dry",
    "Made in USA",
  ], 
},
{
  idItem: "0892310",
  reviewCounter: "2",
  descripton: `The Go To Short is a unique choice for the active adventurer. This versatile short is sustainable, made from a blend of hemp and recycled polyester. Combined, these fibers help reduce water and fossil fuel usage and have a significantly lighter footprint on the earth. Easy on the conscience, but easier still on even the most demanding summer days. Hemp and polyester  are strong and durable, yet lightweight and comfortable enough to wear all day long. Paired with just 3% of spandex, this pull-on short features the  perfect amount of stretch to keep up with you.`,
  details: ["Pull-on styling",
      "Full elastic drawstring waist",
      `10" inseam`,
      "Stretch drawcord",
      "Two slash pockets",
      "Single back pocket",
      "Sustainable style",
      "53% hemp, 44% recycled polyester, 3% spandex",
      "Machine wash cold, tumble dry, cool iron",
      "Designed in the USA",
      "Made in a Fair Trade Certified™ factory in India",
  ], 
},
{
  idItem: "0899980",
  reviewCounter: "4",
  descripton: `Known for bringing you comfort, Ecoths delivers again with the Chambers PJ Pant. These pants are made from 100% cotton, creating a soft-yet-tough flannel that will help regulate your temperature at night, while delivering hypoallergenic comfort. These classic pull-on PJs are secured with an elastic waist and drawcord and feature Buffalo check print reminiscent of winter trips to the cabin. It's time to get cozy!`,
  details:[
    "Classic pull on pajama pant",
    "Elastic waist with drawcord",
    "Checkered print fabric",
    "100% Cotton flannel",
    `31" inseam`,
    "Machine wash cold, tumble dry low, warm iron",
  ], 
},
{
  idItem: "12121212",
  reviewCounter: "6",
  descripton: `These classic nautical cargo pants are built to last and designed 
      to exceed your expectations. Crafted from our stretch twill blend that offers 
      the comfort of cotton, the wrinkle resistance of polyester and the mobility of
       spandex.`,
  details:[
  "Constructed from our 7.5 oz. Mid-Weight Stretch Twill fabric",
  "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets",
  "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
  "Non-roll stretch waistband",
  "Sturdy tunnel belt loops",
  `37" unfinished inseam is ready to be hemmed to your exact measurements`,
  "62% polyester, 35% cotton, 3% spandex blend is travel-friendly and ready to wear straight from the dryer",
  "Machine wash",
  "Imported"
  ], 
},
{
  idItem: "13131313",
  reviewCounter: "6",
  descripton: `These classic nautical cargo pants are built to last and designed 
      to exceed your expectations. Crafted from our stretch twill blend that offers 
      the comfort of cotton, the wrinkle resistance of polyester and the mobility of
       spandex.`,
  details:[
  "Constructed from our 7.5 oz. Mid-Weight Stretch Twill fabric",
  "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets",
  "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
  "Non-roll stretch waistband",
  "Sturdy tunnel belt loops",
  `37" unfinished inseam is ready to be hemmed to your exact measurements`,
  "62% polyester, 35% cotton, 3% spandex blend is travel-friendly and ready to wear straight from the dryer",
  "Machine wash",
  "Imported"
  ], 
},
{
  idItem: "14141414",
  reviewCounter: "6",
  descripton: `These classic nautical cargo pants are built to last and designed 
      to exceed your expectations. Crafted from our stretch twill blend that offers 
      the comfort of cotton, the wrinkle resistance of polyester and the mobility of
       spandex.`,
  details:[
  "Constructed from our 7.5 oz. Mid-Weight Stretch Twill fabric",
  "Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets",
  "Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife",
  "Non-roll stretch waistband",
  "Sturdy tunnel belt loops",
  `37" unfinished inseam is ready to be hemmed to your exact measurements`,
  "62% polyester, 35% cotton, 3% spandex blend is travel-friendly and ready to wear straight from the dryer",
  "Machine wash",
  "Imported"
  ], 
},
]; 
  export  {itemDescription};
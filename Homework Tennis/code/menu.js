//constructor of tooltip class for menu elements
class Tooltip {
    constructor() {
        this.tooltip = document.createElement("div"); // container for tooltip
        this.tooltip.classList.add("tooltip");
    }
// method show - show the text in Tooptip after the option of menu is focused
    show(text, x, y){
        this.tooltip.innerHTML = text;
        this.tooltip.classList.add("tooltip-visible");
        console.log(document.querySelector(".container").offsetWidth);
        console.log(x + parseFloat(this.tooltip.offsetWidth));
        if(x + this.tooltip.offsetWidth > document.querySelector(".container").offsetWidth){
            x = document.querySelector(".container").offsetWidth - this.tooltip.clientWidth; 
        }
        this.tooltip.style.left = x + "px";
        this.tooltip.style.top = y + "px";
        document.body.append(this.tooltip);
    }
// method hide - hide the text with Tooptip after the option of menu is blured
    hide(){
        this.tooltip.classList.remove("tooltip-visible");
    }
}

const mouseOverHandler = function(e) {
    if(!e) e = window.event;
    for(let elem in textTip){
    let c = this.getBoundingClientRect(),
        left = c.left,
        height = c.height,
        top = c.top;
        console.log(height, top)
        if(textTip[elem].name === e.target.innerText){
            text = textTip[elem].text;
        }    
        x = parseFloat(left);
        y = parseFloat(height) + parseFloat(top);
    } 
    tooltip.show(text, x, y);
}

const mouseOutHandler = (e) => {
    if(!e) e = window.event;
    tooltip.hide();
}

const mouseClickHandler = (e) => {
    console.log(e.target.innerText);
//пункт меню - Довідка 
    if(e.target.innerText === "❗"){
        if(!isMenu){ 
            isMenu = true;
            isClick = true;
            const formHelp = document.querySelector(".info-help"),
            btnOkHelp = document.querySelector(".btnOK-help");
            formHelp.style.display = "block";
            btnOkHelp.onclick = () => {
                formHelp.style.display = "none";
                isMenu = false;
                isClick = false;
            }
        }
    }
            
//пункт меню - Інформація про розробника
    if(e.target.innerText === "💁"){
        if(!isMenu){ 
            isMenu = true;
            isClick = true;
            const formInfo = document.querySelector(".info-designer"),
            btnOkInfo = document.querySelector(".btnOK-info-designer");
            formInfo.style.display = "block";
            btnOkInfo.onclick = () => {
                formInfo.style.display = "none";
                isMenu = false;
                isClick = false;
            }
        }
    }        
//пункт меню - Вибрати суперника
    if(e.target.innerText === "💪"){
        if(!isMenu){ 
            isMenu = true;
            isClick = true;
        //визначаємо, що вибрано раніше 
            if(rightPlayer){
                radioRightPlayer.forEach(elem =>{
                    if(elem.value === rightPlayer){
                        elem.checked = true;
                        if(rightPlayer === "player"){
                            labelNameRightPLayer.style.visibility = "visible";
                            inputNameRight.style.visibility = "visible";
                            inputNameRight.placeholder = rightPlayerName;
                        }else{
                            labelNameRightPLayer.style.display = "hidden";
                            inputNameRight.style.visibility = "hidden";
                        } 
                    }else{
                        elem.checked = false;
                    }    
                });
            }
            if(leftPlayer){
                radioLeftPlayer.forEach(elem =>{
                    if(elem.value === leftPlayer){
                        elem.checked = true;
                        if(leftPlayer === "player"){
                            labelNameLeftPLayer.style.visibility = "visible";
                            inputNameLeft.style.visibility = "visible";
                            inputNameLeft.placeholder = leftPlayerName;
                        } else{
                            labelNameLeftPLayer.style.visibility = "hidden";
                            inputNameLeft.style.visibility = "hidden";
                        } 
                    }else{
                        elem.checked = false;
                    }
                console.log(elem);    
                });
            }
            if(!rightPlayer){
                radioRightPlayer.forEach(elem =>{
                    if(elem.checked){
                        if(elem.value === "player"){
                            labelNameRightPLayer.style.visibility = "visible";
                            inputNameRight.style.visibility = "visible";
                            inputNameRight.placeholder = rightPlayerName;
                            if(rightPlayerName !== "" || rightPlayerName !== "Комп'ютер"){
                                inputNameRight.placeholder = rightPlayerName;
                            }else{
                                inputNameRight.placeholder =  "Ігрок 2"
                            }
                        }else{
                            labelNameRightPLayer.style.visibility = "hidden"; 
                            inputNameRight.style.visibility = "hidden";                  
                        } 
                    }
                });
            }
            if(!leftPlayer){
                radioLeftPlayer.forEach(elem =>{
                    if(elem.checked){
                        if(elem.value === "player"){
                            labelNameLeftPLayer.style.visibility = "visible";
                            inputNameLeft.style.visibility = "visible";
                            if(leftPlayerName !== "" || leftPlayerName !== "Комп'ютер"){
                                inputNameLeft.placeholder = leftPlayerName;
                            }else{
                                inputNameLeft.placeholder =  "Ігрок 1";
                            }
                        }else{
                            labelNameLeftPLayer.style.visibility = "hidden";
                            inputNameLeft.style.visibility = "hidden";
                        } 
                    }
                });    
            }
        //show form 
            if(formSelectPlayers.style.display = 'none'){
                formSelectPlayers.style.display = "block";
            }
        //якщо натиснута кнопка X - сховати форму
            btnX.onclick = function(){
                formSelectPlayers.style.display = "none"; // hide form
                isMenu = false;
                isClick = false;
            } 
        //якщо натиснута radio "computer"/"player"- сховати/show input   
            radioRightPlayer.forEach(elem => {
                elem.addEventListener("click", () => {
                    if(elem.value === "computer"){
                        labelNameRightPLayer.style.visibility = "hidden"; 
                        inputNameRight.style.visibility = "hidden";        
                    }else{
                        if(rightPlayerName === "" || rightPlayerName === "Комп'ютер"){
                            inputNameRight.placeholder =  "Ігрок 2"; 
                        }else{
                            inputNameRight.placeholder = rightPlayerName;
                        }
                        labelNameRightPLayer.style.visibility = "visible";
                        inputNameRight.style.visibility = "visible";
                    }
                })
            }); 
            radioLeftPlayer.forEach(elem => {
                elem.addEventListener("click", () => {
                    if(elem.value === "computer"){
                        labelNameLeftPLayer.style.visibility = "hidden";
                        inputNameLeft.style.visibility = "hidden";
                    }else{
                        labelNameLeftPLayer.style.visibility = "visible";
                        inputNameLeft.style.visibility = "visible";
                        if(leftPlayerName === "" || leftPlayerName === "Комп'ютер"){
                            inputNameLeft.placeholder =  "Ігрок 1";
                        
                        }else{
                            inputNameLeft.placeholder = leftPlayerName;
                        }
                    }
                });
            }); 
        //якщо натиснута кнопка ОК - записати дані в LocalStorage та сховати форму   
            btnOk.onclick = function(){
                radioRightPlayer.forEach(elem => {
                    if(elem.checked){
                        window.localStorage.rightPlayer = elem.value;
                        if(elem.value === "player"){
                            if(inputNameRight.value === "" || inputNameRight.value === "Ігрок 2" ){
                                document.querySelector("#player2").innerText = "Ігрок 2";
                                window.localStorage.rightPlayerName = "Ігрок 2";
                            } else{
                                document.querySelector("#player2").innerText = inputNameRight.value;
                                window.localStorage.rightPlayerName =inputNameRight.value;
                            }
                        }   
                        if(elem.value === "computer"){        
                            document.querySelector("#player2").innerText = "Комп'ютер";
                            window.localStorage.rightPlayerName = document.querySelector("#player2").innerText;    
                        } 
                    }
                });
                radioLeftPlayer.forEach(elem =>{
                    if(elem.checked){
                        window.localStorage.leftPlayer = elem.value;
                        if(elem.value === "player"){
                            if(inputNameLeft.value === "" || inputNameLeft.value === "Ігрок 1" ){   
                                document.querySelector("#player1").innerText = "Ігрок 1";
                                window.localStorage.leftPlayerName = "Ігрок 1";
                            } else{
                                document.querySelector("#player1").innerText = inputNameLeft.value;
                                window.localStorage.leftPlayerName =inputNameLeft.value;
                            }
                        }
                        if(elem.value === "computer"){     
                            document.querySelector("#player1").innerText = "Комп'ютер";
                            window.localStorage.leftPlayerName = document.querySelector("#player1").innerText;
                        } 
                    }
                });
            
                formSelectPlayers.style.display = "none"; // hide form
                isMenu = false; 
                isClick = false;   
            }  
        }
    } 
// end of пункт меню - Вибрати суперника
//пункт меню - Таблиця результатів
    if(e.target.innerText === "🏆"){
        if(!isMenu){ 
            isMenu = true;
            isClick = true;
            const tableResults = document.querySelector(".table-results"),
            textAreaResults = document.querySelector("#textarea-results"),
            btnTableResults = document.querySelector(".btnOK-table-results");
            tableResults.style.display = "block";
            textAreaResults.innerHTML = "";
                
            if (window.localStorage.result){ // перевіряємо, чи є дані в localStorage
                results = JSON.parse(window.localStorage.result); //якщо є, то записуємо в масив обєктів
                console.log(results); 
                results.forEach(elem =>{
                //    textAreaResults.append(tableRow);
                //    tableData1.innerText = elem.playerL;
                //    tableRow.append(tableData1);
                const tableData1 = document.createElement("div"),
                    tableData2 = document.createElement("div"),
                    tableData3 = document.createElement("div");
                    tableData1.innerText = elem.playerL;
                    textAreaResults.append(tableData1);    
                    tableData2.innerText =` ${elem.countL}  :  ${elem.countR} `
                    tableData1.after(tableData2);
                    tableData3.innerText = elem.playerR;
                    tableData2.after(tableData3);
            //        textAreaResults.insertAdjacentHTML("afterbegin", 
            //        `<tr>
            //        <td>${elem.playerL}</td>
            //        <td>${elem.countL}</td>
            //        <td>:</td>
            //        <td>${elem.countR}</td>
            //        <td>${elem.playerR}</td>
            //        </tr><br/>`);
            //        console.log(elem.playerL);
                    
                         
            //        textResultString += `${elem.playerL} ${elem.countL} ${elem.countR} ${elem.playerR} <br/>`;
                });
            console.dir( textAreaResults);
    
            } 
            btnTableResults.onclick = () => {
                tableResults.style.display = "none";
                isMenu = false;
                isClick = false;
            }
        }
    }   
// end of пункт меню - Таблиця результатів
    
    //пункт меню - Налаштування
    if(e.target.innerText === "🔧"){
        if(!isMenu){ 
        //    isMenu = true;
        //    isClick = true;
        }
    }  
// end of пункт меню -  Налаштування 
} // end of меню 

window.addEventListener("DOMContentLoaded", () => {

    const [...menu] = document.querySelectorAll(".menu > div");
    
    menu.forEach(item => {
        item.addEventListener('mouseover', mouseOverHandler);
        item.addEventListener('mouseout', mouseOutHandler);
        item.addEventListener('click', mouseClickHandler);
    });
 
    if(!isMenu){
        menu.forEach(item => {
            item.addEventListener('click', mouseClickHandler);
        });
    }
}); 

const tooltip = new Tooltip();
const textTip = [
{name: "❗", name2: `&#128264`, text: "Довідка"},
{name: "💁", name2: `&#128129`, text: "Інформація <br> про розробника"},
{name: "💪", name2: `&#128170`, text: "Вибрати <br> суперника"},
{name: "🏆", name2: `&#127942`, text: "Таблиця <br> результатів"},
{name: "🔧", name2: `&#128295`, text: "Налаштування"}
];


//змінні, які ми маємо бачити будь-де
let isMenu = false;
let isClick = false;
let x, y; 

const formSelectPlayers = document.querySelector(".form-select-players"), 
            labelNameRightPLayer = document.querySelector(".label-name-right-player"), 
            labelNameLeftPLayer = document.querySelector(".label-name-left-player"),
            inputNameRight = document.querySelector("#name-right-player"),
            inputNameLeft = document.querySelector("#name-left-player"),
            btnOk = document.querySelector(".btn-form-select-players"),
            btnX = document.querySelector(".btn-X-form-select-players"),
            radioRightPlayer = [...document.getElementsByName("right-player")],
            radioLeftPlayer = [...document.getElementsByName("left-player")];
     //read data from localStorage        
let rightPlayer, leftPlayer, rightPlayerName, leftPlayerName;
if(window.localStorage.rightPlayer){
rightPlayer = window.localStorage.rightPlayer;
}
if(window.localStorage.leftPlayer){
leftPlayer = window.localStorage.leftPlayer;
}
if(window.localStorage.rightPlayerName){
rightPlayerName = window.localStorage.rightPlayerName;
}
if(window.localStorage.leftPlayerName){
leftPlayerName = window.localStorage.leftPlayerName;
}
            
let results = [];
let playResult = {};
if (window.localStorage.result){ // перевіряємо, чи є дані в localStorage
results = JSON.parse(window.localStorage.result); //якщо є, то записуємо в масив обєктів
console.log(results);  
} 
import {items} from "./data-item.js";
import {colors} from "./data-color.js";
import {setPageName, setSelectGroup, createElement} from "./function.js";

//клік по навігації - "Shorts - Sale"
setPageName();
setSelectGroup();

const selectedGroup = window.sessionStorage.productGroup; //назва групи -selectedGroup-
//console.log(selectedGroup);

//дані по корзинці -userCard- з localStorage
let counterItem;
let userCard =[];
const divCounterCard = document.querySelector(".counter-shopping-cart");
if(!window.localStorage.userCard){
    userCard =[];
    counterItem = 0; 
}else{
    userCard = JSON.parse(window.localStorage.userCard);
    counterItem = userCard.length;
}
divCounterCard.innerText = counterItem;

//фільтр з 'data-item' по обраній категорії (групі) товарів - 
//отримуємо масив -resultCategory-
let resultCategory = items.filter(elem => elem.category === `${(window.sessionStorage.productGroup).toLowerCase()}`);
//console.log("resultCategory = ", resultCategory);
//і записуємо його в sessionStorage
window.sessionStorage.resultCategory = JSON.stringify(resultCategory);

//в масиві -resultCategory- групуємо дані по товарам [по idItem]
let resultName = [], itemColors = [];
let carts = resultCategory.reduce((r, i) =>{
    r[i.idItem] = r[i.idItem] || [];
    r[i.idItem].push(i);
    return r;
}, {});
for(let key in carts) {
    resultName.push(carts[key]);
}
//і записуємо його в sessionStorage
window.sessionStorage.resultName = JSON.stringify(resultName);
//console.log("resultName=", resultName);
//-----------------------------------------------------------------------------
//////вивід на сторінку масивів -button- 
//сортуємо 'resultCategory' по властивості 'color'
const sortColors = resultCategory.sort(function(a,b) { //сортуємо 'resultCategory' по властивості 'color'
    if(a.color > b.color){
        return 1;
    }
    if(a.color < b.color){
        return -1;
    }
    return 0;
});
//знаходимо контейнер для -button-кольорів-
const divColors = document.querySelector("#color");
// і заповнюємо його
let itemColor = "";
for(let i = 0; i < sortColors.length; i++){
    if(itemColor !== sortColors[i].color){
        itemColor = sortColors[i].color;
        colors.forEach(el =>{
            if(el.colorName === itemColor){
                const item = createElement("button", "btn-catalog-select color");
                item.style.backgroundColor = `rgb(${el.rgba})`; //колір для фону
                item.dataset.colorName = el.colorName; //назва кольору
                item.dataset.clicked = false;
                divColors.append(item);   
            }
        });
    }
}

//по властивості 'size'
//сортуємо 'resultCategory' по властивості 'size'
const sortSizes = resultCategory.sort(function(a,b) {
    if(a.sizeItem > b.sizeItem){
        return 1;
    }
    if(a.sizeItem < b.sizeItem){
        return -1;
    }
    return 0;
});
//знаходимо контейнер для -buttons-розмірів-
const divSizes = document.querySelector("#size");
// і заповнюємо його
let itemSize = "";
for(let i = 0; i < sortSizes.length; i++){
    if(itemSize !== sortSizes[i].sizeItem){
        itemSize = sortSizes[i].sizeItem;
        const item = createElement("button", "btn-catalog-select size");
        item.innerText = itemSize; //розмір
        item.dataset.clicked = false;
        divSizes.append(item);     
    }
}
//по властивості 'brand'
//сортуємо 'resultCategory' по властивості 'brand'
const sortBrand = resultCategory.sort(function(a,b) {
    if(a.brand > b.brand){
        return 1;
    }
    if(a.brand < b.brand){
        return -1;
    }
    return 0;
});
//console.log(sortBrand)
//знаходимо контейнер для -buttons-брендів-
const divBrand = document.querySelector("#brand");
// і заповнюємо його
let itemBrand = "";
for(let i = 0; i < sortBrand.length; i++){
    if(itemBrand !== sortBrand[i].brand){
        itemBrand = sortBrand[i].brand;
        const item = createElement("div", "btn-catalog-select brand");
        item.innerText = itemBrand; //бренд 
        item.dataset.clicked = false;
        divBrand.append(item);     
    }
}
//---------------------------------------------------------------
function fillCarts(){
//створюємо карточки для товарів з масиву resultName
//для цього знаходимо контейнер -Cards- куди будемо додавати картки  
const divCards = document.querySelector(".cards"); 
divCards.innerHTML = "";
//і заповнюємо елементи карток даними  
for(let i = 0; i < pageResultName.length; i++){
    let idItem = 0;
    divCards.insertAdjacentHTML("beforeend", pageResultName[i].map((item) =>{
//        console.log(item.idItem, idItem);       
        if( idItem !== item.idItem){
            idItem = item.idItem; 
            return `
            <div class="card catalog-card" id="${item.idItem}">
                <a class="a-card-item" href='#'>
                    <img class="img-card" src=".${item.imageFront}" >
                </a>
                <div class="card-title">${item.itemName}</div>
                <div class="stars">
                    <img class="img-item" src="/image/stars.svg">
                </div>
                <p> As low as 
                    <span class="price-item">$${(parseFloat(item.price)).toFixed(2)}</span>
                </p>
                <div class="card-color">
                    
                </div>
                <div class="btn">
                    <a class="card-btn-a">ADD TO CART</a>
                </div>
            </div> 
            `     }      
   }).join(" "));
}

//заповнюємо div з кольорами в кожній картці товару
//визначаємо кольори для кожного товару і записуємо їх в -itemColors-
    itemColors = []
    for(let i = 0; i < pageResultName.length; i++){
        let colorItem = "", arr = [];
        pageResultName[i].map((item) => {
            if(colorItem !== item.color){
                colorItem = item.color;
    //            console.log(item);
                arr.push(item);
            }
        itemColors[i] = arr;       
    });
    }
//    console.log("itemColors=", itemColors);
    window.sessionStorage.itemColors = JSON.stringify(itemColors);

    //додаємо div з кольором товару в div-контейнер -divCardColor-
    const [...divCardColor] = document.querySelectorAll(".card-color");
    for(let i = 0; i < itemColors.length; i++){
        for(let j = 0; j < itemColors[i].length; j++){
            colors.forEach(el => {
                if(el.colorName === itemColors[i][j].color){
                    const itemCardColor = createElement("div", "item-card-color");
                    itemCardColor.style.backgroundColor = ` rgb(${el.rgba})`; //колір для фону
                    itemCardColor.dataset.colorName = el.colorName; //назва кольору
                    divCardColor[i].append(itemCardColor);     
                }    
            });
        }
    }
}
//фільтрація товарів
const divCardItems = document.querySelectorAll(".card.catalog-card");
const btnSizes = document.querySelectorAll(".btn-catalog-select.size");
const btnColors = document.querySelectorAll(".btn-catalog-select.color");
const btnBrands = document.querySelectorAll(".btn-catalog-select.brand");
//-------------------------------------------------------------
//поділ на сторінки 
let currentPage = 1, currentItem;
const numberItem = 5;  //виводимо по 5 товарів на сторінку
const maxPage =  Math.ceil(resultName.length / numberItem);
//console.log(Math.ceil(resultName.length / numberItem));
let pageResultName = []; 
if(resultName.length  > numberItem * currentPage){
    currentItem = numberItem;
}else{
    currentItem = resultName.length - numberItem * (currentPage -1);
}
//console.log((currentPage - 1)*numberItem);
//console.log(currentItem)

for(let i = 0; i < currentItem; i++){
    pageResultName[i] = resultName[i + (currentPage - 1)*numberItem];
//    console.log(i,(i + (currentPage - 1) * numberItem ))
}
//console.log(pageResultName);
//-------------------------------------------------------------
function clearFilter(){ //remove filter
    unclickedBtn();
    btnClear.style.display = "none";
    btnClear.dataset.clicked = false;
    const divCardItems = document.querySelectorAll(".card.catalog-card");
    divCardItems.forEach(elem =>{
        elem.style.display = "flex";
    });
}

function clickCard(ev){
      const selectItem = resultCategory.filter(item => item.idItem === ev.currentTarget.id);
            console.log(selectItem);
            console.log(ev.currentTarget.id)
        //зберігаємо його в sessionStorage
            window.sessionStorage.selectItem = JSON.stringify(selectItem);
        //i переходимо на сторінку 'product-index.html'    
            document.location.pathname = "../product/index.html";
}

//всі кнопки отримують властивість dataset.clicked = 'false'
function unclickedBtn(){
    btnColors.forEach(el => { 
        el.classList.remove("active"); 
        el.dataset.clicked = false;
    });
    btnSizes.forEach(el => { 
        el.classList.remove("active");
        el.dataset.clicked = false; 
    });
    btnBrands.forEach(el => { 
        el.classList.remove("active"); 
        el.dataset.clicked = false;
    });
}
//hover по кольорах на картках товарів
function hoverColor(){  
    document.querySelectorAll(".card-color").forEach(elem =>{  // контейнер з div-кольорами
        elem.querySelectorAll(".item-card-color").forEach(el =>{  // div-кольори в контейнері
            el.addEventListener("mouseover", (ev) => {
                const indexItem = resultCategory.findIndex(item => // пошук индексу в масиві 
                    item.idItem === elem.parentElement.id && 
                    item.color === ev.currentTarget.dataset.colorName);
            //заміна картинки відповідного кольору       
                elem.parentElement.querySelector(".img-card").src = `.${resultCategory[indexItem].imageFront}`;
            });                    
        });  

        elem.querySelectorAll(".item-card-color").forEach(el =>{ // div-кольори в контейнері
            el.addEventListener("mouseout", (ev) => {
                const indexItem = resultCategory.findIndex(item => // пошук индексу в масиві 
                    item.idItem === elem.parentElement.id && 
                    item.color === elem.querySelectorAll(".item-card-color")[0].dataset.colorName);
            //заміна картинки відповідного кольору       
                    elem.parentElement.querySelector(".img-card").src = `.${resultCategory[indexItem].imageFront}`;
            });                    
        });  
    });
}
//-------------------------------------------------------------
///фільтрація товарів по розміру
function sizeFilter(){
    const divCardItems = document.querySelectorAll(".card.catalog-card");
    const btnSizes = document.querySelectorAll(".btn-catalog-select.size");
    const btnColors = document.querySelectorAll(".btn-catalog-select.color");
    const btnBrands = document.querySelectorAll(".btn-catalog-select.brand");

    btnSizes.forEach(elem => {
        elem.addEventListener("click", (ev) =>{
            unclickedBtn();
            elem.classList.add("active"); 
            elem.dataset.clicked = false;
            btnClear.style.display = "block";
            btnClear.dataset.clicked = true; 
            const select = resultCategory.filter(item => item.sizeItem === ev.target.innerText);
            divCardItems.forEach(el =>{
                if(!select.some(item => item.idItem === el.id) ){
                    el.style.display = "none";
                }else{
                    el.style.display = "flex";   
                }
            });
        });
    });
  
    ////фільтрація товарів по кольору
    btnColors.forEach(elem => {
        elem.addEventListener("click", (ev) =>{
            unclickedBtn();
            elem.classList.add("active"); 
            elem.dataset.clicked = false;
            btnClear.style.display = "block";
            btnClear.dataset.clicked = true; 
            const select = resultCategory.filter(item => item.color === ev.target.dataset.colorName);
            divCardItems.forEach(el =>{
                if(!select.some(item => item.idItem === el.id) ){
                    el.style.display = "none";
                }else{
                    el.style.display = "flex";   
                }
            });
        });
    });
    
    ////фільтрація товарів по бренду
    btnBrands.forEach(elem => {
        elem.addEventListener("click", (ev) =>{
            unclickedBtn();
            elem.classList.add("active"); 
            elem.dataset.clicked = false;
            btnClear.style.display = "block";
            btnClear.dataset.clicked = true; 
            const select = resultCategory.filter(item => item.brand === ev.target.innerText);
                divCardItems.forEach(el =>{
                if(!select.some(item => item.idItem === el.id) ){
                    el.style.display = "none";
                }else{
                    el.style.display = "flex";   
                }
            });
        });
    });
}
//-----------------------------------------------------------------------

fillCarts();
document.querySelectorAll(".card.catalog-card").forEach(el => {
        el.addEventListener("click",   clickCard);  //клік по картці товару
    });
hoverColor();
sizeFilter();
//відміна фільтру
const btnClear = document.querySelector(".clear");
btnClear.addEventListener("click", clearFilter);   //remove filter

//---------------------------------------------------------------------------
//створення контейнеру з сторінками
const nextPageContainer = document.querySelector(".catalog-next-page");
nextPageContainer.innerHTML = "";
const btnPage = createElement("button", "btn-page");
btnPage.innerText = ">";
for(let i = 0; i < maxPage; i++){
    const btnPage = createElement("button", "btn-page");
    btnPage.innerText = i + 1;
    nextPageContainer.append(btnPage);
}   
    nextPageContainer.append(btnPage);
    nextPageContainer.firstElementChild.classList.add("btn-page-active");
//----------------------------------------------------------------------
//перехід на іншу сторінку
const [...btnNextPage] = document.querySelectorAll(".btn-page");
btnNextPage.forEach(elem =>{
    elem.addEventListener("click", (ev) =>{
        const btnClear = document.querySelector(".clear");
        unclickedBtn();
        btnClear.style.display = "none";
         btnClear.dataset.clicked = false;
        
//        btnClear.addEventListener("click", clearFilter);   //remove filter
        document.querySelector(".btn-page-active").classList.remove("btn-page-active");
        elem.classList.add("btn-page-active");    
        if(ev.target.innerText === ">"){
            currentPage = maxPage;
        }else{
            currentPage = ev.target.innerText;
        }  
        if(resultName.length  > numberItem * currentPage){
            currentItem = numberItem;
        }else{
            currentItem = resultName.length - numberItem * (currentPage -1);
        }
       // console.log((currentPage - 1)*numberItem);
       // console.log(currentItem)
        pageResultName = []
        for(let i = 0; i < currentItem; i++){
            pageResultName[i] = resultName[i + (currentPage - 1)*numberItem];
        //    console.log(i,(i + (currentPage - 1) * numberItem ))
        }
//        console.log(pageResultName);
     
        fillCarts();  
        document.querySelectorAll(".card.catalog-card").forEach(el => {
        el.addEventListener("click", clickCard);
        });
        hoverColor();
        sizeFilter();
    });    
});




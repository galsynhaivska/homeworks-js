// The moving ball in the box  --- the example 
const imgField = document.querySelector("#field"),
    ball = document.querySelector(".ball");
let stepX = 1, stepY = 1, 
    leftPos, 
    topPos;

ball.style.left = (imgField.clientWidth / 2 - ball.clientWidth / 2) +"px";
ball.style.top = (imgField.clientHeight / 2 - ball.clientHeight / 2) + "px";
leftPos = imgField.clientWidth / 2 - ball.clientWidth / 2;
topPos = imgField.clientHeight / 2 - ball.clientHeight / 2;
console.log(imgField.clientWidth / 2);
console.log(imgField.clientHeight / 2);
console.log(leftPos);
console.log(topPos);
console.log(imgField.offsetLeft);
console.log(imgField.offsetTop);
window.onload = () =>{
    setInterval(animate, 10);
}

const animate = () => {
    ball.style.left = leftPos + "px";
    ball.style.top = topPos + "px";
    leftPos += stepX;
    if(leftPos >= imgField.clientWidth - ball.clientWidth){
        stepX = -1;
    }
    if(leftPos <= 0){
        stepX = 1;
    }
    topPos += stepY;
    if(topPos >= imgField.clientHeight - ball.clientWidth){
        stepY = -1;
    }
    if(topPos <= 0){
        stepY = 1;
    }
}




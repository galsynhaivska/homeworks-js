/*4. Создай кнопки 1,2,3,4 и при нажатии на кнопку 
выведи информацию о том, какая кнопка была нажата
*/
window.addEventListener("DOMContentLoaded", () => {
    const [...buttons] = document.querySelectorAll(".task04-btn"),
        div = document.createElement("div");
        div.classList.add("task-04");

    buttons.forEach(item =>{
        item.addEventListener("click", (e) => {
            buttons[buttons.length-1].after(div);
            div.innerText =`Ви натиснули кнопку ${item.innerText}`; 
        });
    });
});
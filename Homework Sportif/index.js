const category = [
    'shorts',
    'pants',
    'shirts',
    'accessories',
    'sales',
];

const brands = [
    'Sportif USA',
    'Weekender',
    'Ecoths',
    'Hook&Tackle',
    'Tori Richard',
];

const colors = [
        {colorName: 'navy', hex: '#000080', rba: '0, 0, 128'},
        {colorName: 'blue', hex: '#0000FF', rba: '0, 0, 255'},
        {colorName: 'khaki', hex: '#F0E68C', rba: '240, 230, 140'},
        {colorName: 'desert', hex: '#D2B48C', rba: '210, 180, 140'},
        {colorName: 'black', hex: '#000000', rba: '0, 0, 0'},
        {colorName: 'olive', hex: '#808000', rba: '128, 128,0'},
        {colorName: 'pewter', hex: '#000080', rba: '0, 0, 128'},
        {colorName: 'green', hex: '#008000', rba: '0, 128, 0'},
        {colorName: 'white', hex: '#FFFFFF', rba: '255, 255, 255'},
        {colorName: 'natural', hex: '#FAF0E6', rba: '250, 240, 230'},
        {colorName: 'gray', hex: '#808080', rba: '128, 128, 128'}
];


const sizes = [30, 32, 34, 36, 38, 40, 42, 44, 46, 48];
const inseams = ['4.0', '5.0', '6.0', '7.5', '8.0', '8.5'];



const item = [{
    id: 670170,
    category: 'shorts', 
    brand: 'Sportif USA',
    name: "Sportif's Original Short",
    imageFront: "./data-image/670170_01.ipg",
    imageBack: './data-image/670170_01_back.ipg',
    color: 'khaki',
    inseam: "4.0",
    price: 72,
    size: 30 ,
    quantity: 5,
    descripton: `With its classic design, practical features and unequaled comfort -
         it's no wonder Sportif 'Original' stretch cargo shorts are practically the 
         uniform on yachts worldwide! A customer favorite for over 50 years, they're 
         called the world's best boating shorts. Featuring a 4" inseam for sizes 30-40 
         and 5.5" inseam for sizes 42-46.`,
    reviews: '0',     
    details: `Constructed from our 7.5 oz. Original Stretch Twill fabric
    Exclusive 7-pocket design includes two deep front pockets, two front cargo pockets with button-through flap closure, and two zippered security back pockets
    Bonus pocket inside the right front pocket is perfect for carrying a small cell phone or pocket knife
    Non-roll stretch waistband
    Sturdy tunnel belt loops
    Durable double fabric seat
    Gusset crotch
    Double-stitched seams throughout
    Bar tack stitching reinforces areas of stress to guarantee years of service
    4" inseam (sizes 30-40) or 5.5" inseam (sizes 42-46)
    Wrinkle resistant 62% polyester, 35% cotton, 3% spandex blend is travel friendly and ready to wear straight from the dryer
    Machine wash
    Imported
    `,
},

];


function showSlide(){
    viewSliders[viewSlide].style.backgroundColor = "red";
    if(viewSlide < viewSliders.length - 1) {
        viewSlide++;
    }else{
        viewSlide = 0;
    }
    viewSliders[viewSlide].style.backgroundColor = "green";
    viewImages.animate({'marginLeft': -viewSlide * $('#viewport').width() + "px"}, {'duration': 500});
}


const slideCount = $('#slider').children().length;
const viewImages = $('.image-slider');
const viewSliders = $('.viewSlide');
let viewSlide = 0;

//setInterval and stop when hover
const slideInterval = 3000;
$(document).ready(function(){
    let switchInterval = setInterval(showSlide, slideInterval);
    $('#viewport').hover(function(){
        clearInterval(switchInterval);
    }, function() {
        switchInterval = setInterval(showSlide, slideInterval);
});
});

// click on button next
viewSliders[viewSlide].style.backgroundColor = "green";
$(".next").click(function() {
    viewSliders[viewSlide].style.backgroundColor = "red";
    if(viewSlide < viewSliders.length - 1) {
        viewSlide++;
    }else{
        viewSlide = 0;
    }
    viewSliders[viewSlide].style.backgroundColor = "green";
    viewImages.animate({'marginLeft': -viewSlide * $('#viewport').width() + "px"}, {'duration': 500});
});
// click on button prev
$(".prev").click(function() {
    viewSliders[viewSlide].style.backgroundColor = "red";
    if(viewSlide > 0) {
        viewSlide--;
    }else{
        viewSlide = viewSliders.length - 1;
    }
    viewSliders[viewSlide].style.backgroundColor = "green";
    viewImages.animate({'marginLeft': - viewSlide * $('#viewport').width() + "px"}, {'duration': 500});
});






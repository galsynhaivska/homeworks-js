import {items} from "./data-item.js";
import {colors} from "./data-color.js";
import {setPageName, setSelectGroup} from "./function.js";

//клік по навігації - "Shorts - Sale"
setPageName();
setSelectGroup();
document.querySelector(".select-group").innerText = "> Your Cart";
//userCart data from localStorage
let counterItem;
let userCard =[];
const divCounterCard = document.querySelector(".counter-shopping-cart");
const emptyBag = document.querySelector(".empty-bag"),
     fullBag = document.querySelector(".full-bag");
     
if(!window.localStorage.userCard){
    userCard =[];
    counterItem = 0;
    divCounterCard.innerText = counterItem;
    emptyBag.style.display = "block";
    fullBag.style.display = "none";
}else{
    userCard = JSON.parse(window.localStorage.userCard);
    counterItem = userCard.length;   
    emptyBag.style.display = "none";
    fullBag.style.display = "block";
    divCounterCard.innerText = counterItem;
//create table
let total = 0;
const tbody = document.querySelector("table tbody");
tbody.innerHTML = ""
tbody.insertAdjacentHTML("beforeend", userCard.map((item, index) => {
    total +=  parseFloat(item.price) * parseInt(item.quantity);
    return `    
    <tr class="tr-item-info">
                        <td class="td-foto-info" >
                            <img class="foto-item" src=.${item.imageFront} alt="foto-item">
                        <td class="td-info-item">
                            <p class="info-brend">${item.brand}</p>
                            <h2 class="info-name">${item.itemName}</h2>
                            <p class="info-color">Color: ${item.color}</p>
                            <p class="info-size">Size: ${item.sizeItem}</p>
                            <button type="button" class="btn-change">Change</button>
                        </td>    
                        <td class="td-price-item"> <p>$${parseFloat(item.price).toFixed(2)}</p></td>
                        <td class="td-quantity">
                            <div class="td-quantity-container">
                                <button type="button" class="btn-decrease"> > </button>
                                <p class="p-quantity">${item.quantity}</p>
                                <button type="button" class="btn-increase"> > </button>
                            </div>
                        </td>
                        <td class="td-total-item">
                            <div class="td-total-container">
                                <p class="p-total">$${(parseFloat(item.price) * parseFloat(item.quantity)).toFixed(2) }</p>
                                <button type="button" class="btn-delete-item"> x </button>
                            </div>
                        </td>
                    </tr>`
    }).join(""));

const spanTotal = document.querySelector(".div-total-sum");
spanTotal.innerText = `$${total.toFixed(2)}`;
}  

//remove item from userCard --> click on button "x"
[...document.querySelectorAll(".btn-delete-item")].forEach(elem =>{
    elem.addEventListener("click", (ev) => {
        const index = [...document.querySelectorAll(".btn-delete-item")].findIndex(item => item === ev.target); 
        console.log(index);
        console.log(userCard[index]);
        userCard = JSON.parse(window.localStorage.userCard);
        userCard.splice(index,1);
        console.log(userCard);
        [...document.querySelectorAll(".tr-item-info")][index].remove();
        window.localStorage.userCard = JSON.stringify(userCard);
        counterItem = userCard.length;
        divCounterCard.innerText = counterItem;
        if(counterItem === 0){
            fullBag.style.display = "none";
            emptyBag.style.display = "block";
        }        
    });
});

//decrease quantity by 1
[...document.querySelectorAll(".btn-decrease")].forEach(elem => {
    elem.addEventListener("click", (ev) =>{
        const index = [...document.querySelectorAll(".btn-decrease")].findIndex(item => item === ev.target); 
        userCard = JSON.parse(window.localStorage.userCard);
        console.log(index);
        console.log(userCard);
        console.log(userCard[index].quantity)
        if(parseInt(userCard[index].quantity) !== 1){
            userCard[index].quantity = parseInt(userCard[index].quantity) - 1;
            document.querySelectorAll(".p-quantity")[index].innerText = userCard[index].quantity
            window.localStorage.userCard = JSON.stringify(userCard);
        }
    });
});

//increase quantity by 1
[...document.querySelectorAll(".btn-increase")].forEach(elem => {
    elem.addEventListener("click", (ev) =>{
        const index = [...document.querySelectorAll(".btn-increase")].findIndex(item => item === ev.target); 
        userCard = JSON.parse(window.localStorage.userCard);
        console.log(index);
        console.log(userCard);
        console.log(userCard[index].quantity)
        if(parseInt(userCard[index].quantity) !== 0){
            userCard[index].quantity = parseInt(userCard[index].quantity) + 1;
            document.querySelectorAll(".p-quantity")[index].innerText = userCard[index].quantity
            window.localStorage.userCard = JSON.stringify(userCard);
        }
    });
});


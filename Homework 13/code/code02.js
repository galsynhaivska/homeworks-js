/* 2. Дано інпути. Зробіть так, щоб усі інпути при втраті фокусу перевіряли свій вміст 
на правильну кількість символів.
 Скільки символів має бути в інпуті, зазначається в атрибуті data-length. 
 Якщо вбито правильну кількість, 
 то межа інпуту стає зеленою, якщо неправильна – червоною.*/

window.addEventListener("DOMContentLoaded", () => {
    const [...inputs] = document.querySelectorAll(".length");
    //inputs[0] => data-length = "10"; inputs[1] => data-length = "3"; 
    inputs.forEach(elem => {
        elem.addEventListener("blur", function(e) {
            if(e.target.value.length !== parseInt(e.target.dataset.length)){
                e.target.style.border = "solid 3px red";
            }else{
                e.target.style.border = "solid 3px green";
            };
        });
    });
});


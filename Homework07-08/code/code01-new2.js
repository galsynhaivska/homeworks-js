/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага,
водій типу Driver, мотор типу Engine.
Методи start(), stop(), turnRight(), turnLeft(),
які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.
Створити похідний від Car клас - Lorry (вантажівка), 
що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, 
який також характеризується граничною швидкістю.
*/

class Driver {
    constructor(lastName, firstName, middleName, experience){
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.experience = experience;
    }
}

class Engine extends Driver{
    constructor(lastName, firstName, middleName, experience, motorPower, producer ){
        super(lastName, firstName, middleName, experience);
        this.motorPower = motorPower;
        this.producer = producer;
    }
}

class Car extends Engine{
    constructor(lastName, firstName, middleName, experience, motorPower, producer, 
                carBrand, carClass, carWeight){
        super(lastName, firstName, middleName, experience, motorPower, producer );
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.carWeight = carWeight;
    }    
        start() { 
            return "Поїхали";
        }
        stop() {
            return "Зупиняємося";
        }    
        turnRight() {
            return "Поворот праворуч";
        }
        turnLeft() {
            return "Поворот ліворуч";
        }
        toString() {
            return `<br> 
            Інформація про автомобіль: <br>
            марка автомобіля ${this.carBrand} <br>
            клас автомобіля ${this.carClass}<br> 
            вага ${this.carWeight}<br>
            мотор ${this.motorPower}, виробник ${this.producer} <br>
            водій ${this.lastName} ${this.firstName} ${this.middleName} <br>
            стаж водіння ${this.experience} <br> `
        }
}

class SportCar extends Car {
    constructor(lastName, firstName, middleName, experience, motorPower, producer, carBrand, carClass, carWeight, maxSpeed){
        super(lastName, firstName, middleName, experience, motorPower, producer, carBrand, carClass, carWeight);
        this.maxSpeed = maxSpeed;
    }
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass} <br>
        вага ${this.carWeight}<br>
        мотор ${this.motorPower}, виробник ${this.producer} <br>
        водій ${this.lastName} ${this.firstName} ${this.middleName} <br>
        стаж водіння ${this.experience} <br> 
        гранична швидкість ${this.maxSpeed} <br>`
    }    
}
        
class Lorry extends Car {
    constructor(lastName, firstName, middleName, experience, motorPower, producer, carBrand, carClass, carWeight, capacity){
        super(lastName, firstName, middleName, experience,motorPower, producer, carBrand, carClass, carWeight);
        this.capacity = capacity;
    }
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass}<br> 
        вага ${this.carWeight}<br>
        мотор ${this.motorPower}, виробник ${this.producer} <br>
        водій ${this.lastName} ${this.firstName} ${this.middleName} <br>
        стаж водіння ${this.experience} <br> 
        вантажопідйомність ${this.capacity} <br>`
    }    
}


const driver1 = new Driver("Ivaniv", "Petro", "Marianovych", 7);
const driver2 = new Driver("Petriv", "Mark", "Serhijovych", 2);
const driver3 = new Driver("Volyk", "Victor", "Petrovych", 12);     
                    
const engine1 = new Engine("Ivaniv", "Petro", "Marianovych", 7, "135 kW", "BMW" );
const engine2 = new Engine("Petriv", "Mark", "Serhijovych", 2, "103kW", "Hyundai" ); 
const engine3 = new Engine("Volyk", "Victor", "Petrovych", 12, "120 kW", "Porsche" );
                

const car1 = new Car("Ivaniv", "Petro", "Marianovych", 7, "135 kW", "BMW" , "BMW G20", "D", "1800");
const car2 = new Car("Petriv", "Mark", "Serhijovych", 2, "103kW", "Hyundai",  "Hyundai", "S", "2500"); 
const car3 = new Car("Volyk", "Victor", "Petrovych", 12, "120 kW", "Porsche" ,  "Porsche 911", "S", "1500"); 

const sportCar1 = new SportCar("Ivaniv", "Petro", "Marianovych", 7, "135 kW", "BMW" , "BMW G20", "D", "1800", "306 km/h");
const lorry1 = new Lorry("Petriv", "Mark", "Serhijovych", 2, "103kW", "Hyundai",  "Hyundai", "S", "2500", "4600 kg");

console.log(driver1, driver2, driver3);
console.log(engine1, engine2, engine3);
console.log(car1, car2, car3);

console.log(sportCar1);
console.log(lorry1);


console.log(`${car1.carBrand} : ${car1.turnLeft()}`);
console.log(`${car2.carBrand} : ${car2.start()}`);
console.log(`${car3.carBrand} : ${car3.turnRight()}`);
console.log(`${sportCar1.carBrand} : ${sportCar1.stop()}`);
console.log(`${lorry1.lastName} : ${lorry1.stop()}`);
document.write([car1,car2, car3].join(" <br>").toString(), "<br>", sportCar1.toString(), "<br>", lorry1.toString());

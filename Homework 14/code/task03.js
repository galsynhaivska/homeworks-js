/* 3. Создай див высотой 400 пикселей и добавь на него событие наведения мышки.
    При наведении мышки выведите текстом
    координаты, где находится курсор мышки
    */
window.addEventListener("DOMContentLoaded", () => {
    const divForMouse = document.createElement("div"),
        infoField =   document.createElement("div"),
        divItem = document.querySelector(".div-list-items");
    divForMouse.classList.add("div-mouse");
    divForMouse.innerText = "Поле для мишки, task 03";
    divItem.append(divForMouse);
    divForMouse.addEventListener("mousemove", (e) =>{
    //    console.log("X: ", e.clientX, "Y: ", e.clientY);
        divForMouse.before(infoField);
        infoField.innerText = `clientX: ${e.clientX}, clientY: ${e.clientY} 
            offsetX: ${e.offsetX}, offsetY: ${e.offsetY}`;
    });
});

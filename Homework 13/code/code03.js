import city from "./uacity.js";

/* 3. Використовуючи бібліотеку bootstrap 
створіть форму у якій запитати у користувача данні.
- Ім'я, Прізвище (Українською)
- Список з містами України 
- Номер телефону у форматі +380XX XXX XX XX - 
-- Визначити код оператора та підтягувати логотип оператора. 
- Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌, 
якщо помилки немає, показати зелену галочку ✅.
Перевіряти данні на етапі втрати фокуса 
та коли йде натискання кнопки відправити дані.
*/

// перевірка тексту в інпуті
function validate (patern, text){
    return patern.test(text);
}

// перевірка тексту в інпуті по події 
function validateInput(elem){
    let found = pattern.find(e => e.id === elem.id);
        if(!validate(found.text, elem.value)){
                elem.nextSibling.remove();
                validation.innerText = "❌";
                elem.insertAdjacentText("afterend", "❌");
                if(elem.id === "tel" && elem.previousSibling){
                    console.log(elem.previousSibling);
                    logo.style.visibility = "hidden";
                }
            }else{
                elem.nextSibling.remove();
                validation.innerText = "✅";
                elem.insertAdjacentText("afterend", "✅");
                if(elem.id === "tel"){
                    const operator = operators.find(e => e.id === elem.value.slice(3,6));
                    // displsy <img>
                    logo.src = operator.png;
                    logo.style.visibility = "visible";
            }
            }            
        }

const operators = [
    {id: '067', png: "./img/kyivstar.png"},
    {id: '068', png: "./img/kyivstar.png"},
    {id: '096', png: "./img/kyivstar.png"},
    {id: '097', png: "./img/kyivstar.png"},
    {id: '098', png: "./img/kyivstar.png"},
    {id: '050', png: "./img/vodafone.png"},
    {id: '066', png: "./img/vodafone.png"},
    {id: '095', png: "./img/vodafone.png"},
    {id: '099', png: "./img/vodafone.png"},
    {id: '063', png: "./img/lifecell.png"},
    {id: '093', png: "./img/lifecell.png"},
    {id: '073', png: "./img/lifecell.png"}
];

//масив шаблонів-патернів
const pattern = [
    {id: "first-name", text: /^[А-я-ІіЇїҐґЄє]+$/ },
    {id: "last-name", text: /^[А-я-ІіЇїҐґЄє]+$/ },
    {id: "city", text: /^[-A-z]+$/ },
    {id: "tel", text: /^\+380[0-9]{9}$/ },
    {id: "mail", text: /^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/ }
];   

const form = document.querySelector(".user-data"); //звертаємось до форми  
form[0].placeholder = "Іван";
form[1].placeholder = "Іваненко";
form[2].placeholder = "Dnipro";
form[3].placeholder = "+380771112233";
form[4].placeholder = "sq-t111@gmail.com";

const logo = document.createElement("img"); // create <img>
form[3].insertAdjacentElement("beforebegin", logo);
logo.classList.add("img-logo");

//створюємо список міст 
const dataCity = form.querySelector("#data-city");
city.forEach(element => {
    const option = document.createElement("option");
    option.value = element.city;
    dataCity.append(option);
});


const [...inputs] = form.querySelectorAll(".input"), //звертаємось до інпутів
    validation = document.createElement("span");  //цей елемент будемо додавати чи видаляти при перевірці
//перевірка інпутів
inputs.forEach(elem =>{
    elem.addEventListener("change", function(e) {
        validateInput(elem);
    });         
});

//перевірка всієї форми по натиску на кнопку 'submit'
form.addEventListener("submit", function(e) {
    e.preventDefault();
    for(let i = 0; i < inputs.length ; i++){
        inputs[i].nextSibling.remove();
    }
    for(let i = 0; i < inputs.length ; i++) {
        if(!inputs[i].value || !validate(pattern[i].text, inputs[i].value)){
            validation.innerText = "❌";
            inputs[i].insertAdjacentText("afterend", "❌");
            console.log(console.log("❌ ", inputs[i].value));
        }else{
            validation.innerText = "✅";
            inputs[i].insertAdjacentText("afterend", "✅");
            console.log("✅ ", inputs[i].value);
        }
    }
});

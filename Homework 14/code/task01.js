/* 1. Создай класс, который будет создавать пользователей с именем и фамилией.
     Добавить к классу метод для вывода имени и фамилии */
class User{
    constructor(firstName, lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
    show(){
        return `You input: ${this.firstName} ${this.lastName}`;
    }
}
window.addEventListener("DOMContentLoaded", () => {
    const form = document.querySelector(".form"),
        divOutput = document.createElement("div")
        isSubmit = false;
// css-styles for div with input-user-data
    divOutput.classList.add("div-output");
// form[2] - button "Submit"
    form[2].addEventListener("click", function(e){
        if(!isSubmit){
            isSubmit = true;
//            console.log(form[0].value, form[1].value);
            let user = new User();
            user.firstName = form[0].value;
            user.lastName = form[1].value;
            form.append(divOutput);
            divOutput.innerText = user.show();
//            console.log(user);
            e.preventDefault();
        }else{
            e.preventDefault();
        }
    });    
  
    form[3].addEventListener("click", function(e){
        if(divOutput){
            divOutput.remove();
        }
        form[0].value = "";
        form[1].value ="";
        isSubmit = false;
    });
 

});





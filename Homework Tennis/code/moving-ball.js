window.addEventListener("DOMContentLoaded", () =>{
    // the ball is moving left 
    function moveLeft(border){
        random =  Math.random();
        if(leftPos >= border) {
            height = (imgField.clientHeight  - ball.clientHeight) * random;
            tan = (height - topPos) / border ;
            stepY  = step * tan;
            stepX = -step;
        }
    }    
    //the ball is moving right 
    function moveRight(border){
        random =  Math.random();
        if(leftPos <= border) {
            height = (imgField.clientHeight  - ball.clientWidth) * random;
            tan = (height - topPos) / (imgField.clientWidth - border);
            stepY  = step * tan;
            stepX = step;
        }
    }
    // перша траєкторія
    function startStepBall(){
        random =  Math.random();
        height = (imgField.clientHeight  - ball.clientHeight) * random;
        tan = (height - topPos) / (imgField.clientWidth  - ball.clientWidth);
        stepX = step;
        stepY = step * tan; 
    }
    //спочатку м'ячик знаходиться в центрі поля   
    function startPosition(){
        leftPos = imgField.clientWidth / 2  - ball.clientWidth / 2;
        topPos = imgField.clientHeight / 2 - ball.clientHeight / 2;
        ball.style.left = leftPos + "px";
        ball.style.top = topPos + "px";
        rightRocketTopPos = imgField.clientHeight / 2 - rightRocket.clientHeight / 2;
        leftRocketTopPos = imgField.clientHeight / 2 - rightRocket.clientHeight / 2; 
        rightRocket.style.top = rightRocketTopPos + "px";
        leftRocket.style.top = leftRocketTopPos + "px";
        countRight = 0;
        countLeft = 0;
    }
     //чи закінчуємо гру
    function finishOrNot(){ 
        const divEscape = document.querySelector(".div-escape"),
            btnEscapeYes = document.querySelector(".btn-escape-ok"),
            btnEscapeNo = document.querySelector(".btn-escape-no");
            divEscape.style.display = "block";    
        btnEscapeNo.onclick = () => {
            divEscape.style.display = "none";
        }
        btnEscapeYes.onclick = () => {
            clearInterval(timerId);
            isClick = false;
            btnStart.classList.toggle("start-click");
            startPosition(); 
            countRight = 0;
            countLeft = 0;
            divCountRight.innerText  = "0";
            divCountLeft.innerText  = "0";
            divEscape.style.display = "none";
        }
    }
    //закінчення гри
    function finishRound(countRight,countLeft){ 
        clearInterval(timerId);
        showResult();
        isClick = false;
        btnStart.classList.toggle("start-click");
        console.log(`Гру завершено з рахунком ${countLeft} :  ${countRight}`);
    }

    //запис результату гри в localStorage
    function writingResult(leftPlayerName, countRight, countLeft,rightPlayerName){
        console.log(countLeft, countRight);
    //перезаписуємо results    
        if (window.localStorage.result){
            results = JSON.parse(window.localStorage.result);
            console.log(results);  
        } 
    //створюємо об'єкт    
        playResult = {
            playerL: leftPlayerName,
            countL: countLeft,
            playerR: rightPlayerName,
            countR: countRight
            };
        console.log(playResult);
    //додаємо його в масив results
        results.push(playResult);
        console.log(results);
    //записуємо масив в localStorage    
        window.localStorage.result = JSON.stringify(results);
    }

    //відображення повідомлення про результат гри, підготовка до наступного раунду 
    function showResult(){
        const divResult = document.querySelector(".show-result"),
         pResult = document.querySelector(".show-result > div"),
         btnResult = document.querySelector(".btn-show-result");

        pResult.innerText = `Гру завершено з рахунком \n ${countLeft} :  ${countRight}`;
        divResult.style.visibility = "visible";
        btnResult.addEventListener("click", () => {
            divResult.style.visibility = "hidden";
            countRight = 0;
            countLeft = 0;
            divCountRight.innerText  = "0";
            divCountLeft.innerText  = "0";
        });
    };
// main block
    const imgField = document.querySelector("#field"),
        ball = document.querySelector(".ball"),
        leftRocket = document.querySelector(".left-rocket"),
        rightRocket = document.querySelector(".right-rocket"),
        btnStart = document.querySelector(".start"),
        divCountLeft = document.querySelector(".count-left"),
        divCountRight = document.querySelector(".count-right"),
        divPlayerLeft = document.querySelector("#player1"),
        divPlayerRight = document.querySelector("#player2");
    //for window - input names of players    
    const inputName = document.querySelector(".input-name"),
        dataInput = document.querySelector(".input-data"),
        btnInputName = document.querySelector(".btn-input-name");
        spanName = document.createElement("span");    
    
    //параметри в налаштуванні, які визначає користувач
    const step = 20; // step - швидкість мячика - задавати будемо в налаштуваннях
    const stepRocket = 20; // крок в рх переміщення ракетки
    const maxCount = 5; // кількість очок, коли зупиняється партія 
    //координати м'ячика і ракеток     
    let leftPos,  topPos, 
        rightRocketTopPos, leftRocketTopPos, 
    //лічильники гравців    
        countRight = 0,
        countLeft = 0,
    //траєкторія
        random, height,
        tan, stepX, stepY,
    //таймер і блок на кнопку Старт   
        timerId;
    //    isClick = false,
    
    //write names in div  "#player1", "#player2"  
    if(rightPlayerName !== undefined){
        divPlayerRight.innerText = rightPlayerName; 
    }
    if(leftPlayerName !== undefined){
        divPlayerLeft.innerText = leftPlayerName; 
    }
    
    startPosition();  //спочатку м'ячик знаходиться в центрі поля    
    startStepBall(); // перша траєкторія

    console.log(ball.offsetTop);    
    console.log(random);
    console.log(height); 
    console.log(tan);    
    //події на клавіші - rightPlayer - KeyA,KeyZ; leftPlayer - ArrowUp, ArrowDown 
    document.querySelector("#main").addEventListener("keydown", (e) => {
        if(e.code == "Escape"){
            console.log("isMenu=",isMenu);
            console.log("isClick=",isClick);
            if(!isMenu && isClick) finishOrNot();
        
        };
        if(rightPlayer === "player"){    
            if(e.code === "ArrowUp"){
                if(rightRocketTopPos > 0.1 * rightRocket.clientHeight){
                    rightRocketTopPos -= stepRocket;
                }
            }
            if(e.code === "ArrowDown"){
                if(rightRocketTopPos <= imgField.clientHeight - 1.1 * rightRocket.clientHeight){
                rightRocketTopPos += stepRocket;
                }
            }
            rightRocket.style.top = rightRocketTopPos + "px";
        }
        if(leftPlayer === "player"){
            if(e.code === "KeyA"){
                if(leftRocketTopPos > 0.1 * leftRocket.clientHeight){
                    leftRocketTopPos -= stepRocket;
                }
            }
            if(e.code === "KeyZ"){
                if(leftRocketTopPos <= imgField.clientHeight - 1.1 * leftRocket.clientHeight){
                leftRocketTopPos += stepRocket;
                }
            }
            leftRocket.style.top = leftRocketTopPos + "px";
        };
    });

//show form SelectPlayer before the first running
    if(!leftPlayer || !rightPlayer){
    //show form 
        if(formSelectPlayers.style.display = 'none'){
            formSelectPlayers.style.display = "block";
            isMenu = true;
            isClick = true;
            [...document.getElementsByName("right-player")].forEach(elem =>{
                if(elem.checked){
                    if(elem.value === "player"){
                        inputNameRight.style.visibility = "visible";
                    }else{
                        inputNameRight.style.visibility = "hidden";                    
                    } 
                }
            });
            [...document.getElementsByName("left-player")].forEach(elem =>{
                if(elem.checked){
                    if(elem.value === "player"){
                        inputNameLeft.style.visibility = "visible";
                    } else{
                        inputNameLeft.style.visibility = "hidden";
                    } 
                }
            });    
        }
   //якщо натиснута radio "computer"/"player"- сховати/show input   
    radioRightPlayer.forEach(elem => {
        elem.addEventListener("click", () => {
            if(elem.value === "computer"){
                labelNameRightPLayer.style.visibility = "hidden"; 
                inputNameRight.style.visibility = "hidden";        
            }else{
                labelNameRightPLayer.style.visibility = "visible";
                inputNameRight.style.visibility = "visible";
            }
        })
    }); 
    radioLeftPlayer.forEach(elem => {
        elem.addEventListener("click", () => {
            if(elem.value === "computer"){
                labelNameLeftPLayer.style.visibility = "hidden";
                inputNameLeft.style.visibility = "hidden";
            }else{
                labelNameLeftPLayer.style.visibility = "visible";
                inputNameLeft.style.visibility = "visible";
            }
        })
    });  
     //якщо натиснута кнопка X - сховати форму, leftRocket - computer, rightRocket - player
     btnX.onclick = function(){
        formSelectPlayers.style.display = "none"; // hide form
        isMenu = false;
        isClick = false;
    //leftPlayer    
        document.querySelector("#player1").innerText = "Комп'ютер";
        window.localStorage.leftPlayerName = "Комп'ютер";
        leftPlayer = "computer";
        window.localStorage.leftPlayer = leftPlayer; // "player"/"computer"
        leftPlayerName = window.localStorage.leftPlayerName; // name of left-player 
    //rightPlayer
        document.querySelector("#player2").innerText = "Ігрок 2";
        window.localStorage.rightPlayerName = "Ігрок 2";
        rightPlayer =  "player";
        rightPlayerName = window.localStorage.rightPlayerName;// name of left-player
    }   
    //якщо натиснута кнопка ОК - записати дані в LocalStorage та сховати форму   
        btnOk.onclick = function(){
            [...document.getElementsByName("right-player")].forEach(elem =>{
                if(elem.checked){
            //        console.dir(elem, elem.checked); 
                    window.localStorage.rightPlayer = elem.value;
                    if(elem.value === "player"){
                        if(inputNameRight.value === "" || inputNameRight.value === "Ігрок 2" ){
                            document.querySelector("#player2").innerText = "Ігрок 2";
                            window.localStorage.rightPlayerName = "Ігрок 2";
                        } else{
                            document.querySelector("#player2").innerText = inputNameRight.value;
                            window.localStorage.rightPlayerName =inputNameRight.value;
                        }
                    } 
                    if(elem.value === "computer"){        
                        document.querySelector("#player2").innerText = "Комп'ютер";
                        window.localStorage.rightPlayerName = "Комп'ютер";    
                    }   
                }
            
            });
            [...document.getElementsByName("left-player")].forEach(elem =>{
                if(elem.checked){
                //    console.dir(elem, elem.checked); 
                    window.localStorage.leftPlayer = elem.value;
                  
                    if(elem.value === "player"){
                        if(inputNameLeft.value === "" || inputNameLeft.value === "Ігрок 1" ){   
                            document.querySelector("#player1").innerText = "Ігрок 1";
                            window.localStorage.leftPlayerName = "Ігрок 1";
                        } else{
                            document.querySelector("#player1").innerText = inputNameLeft.value;
                            window.localStorage.leftPlayerName =inputNameLeft.value;
                        }               
                    } 
                    if(elem.value === "computer"){     
                        document.querySelector("#player1").innerText = "Комп'ютер";
                        window.localStorage.leftPlayerName = "Комп'ютер";
                    }
                }

            });
        formSelectPlayers.style.display = "none"; // hide form
        isMenu = false;
        isClick = false; 
        leftPlayer = window.localStorage.leftPlayer; // "player"/"computer"
        leftPlayerName = window.localStorage.leftPlayerName; // name of left-player
        rightPlayer = window.localStorage.rightPlayer; 
        rightPlayerName = window.localStorage.rightPlayerName;// name of left-player
        }       
       
    }

//початок гри по кліку на кнопку Старт
    btnStart.addEventListener("click", () => {
        if(!isClick){
            isClick = true;
            leftPlayer = window.localStorage.leftPlayer; // "player"/"computer"
            rightPlayer = window.localStorage.rightPlayer; 
            leftPlayerName = window.localStorage.leftPlayerName; 
            rightPlayerName = window.localStorage.rightPlayerName;
            console.log(leftPlayer, rightPlayer);
            btnStart.classList.add("start-click");
            timerId = setInterval(animate, 50); 
        }
    });
//блок animate - тут все, що стосується поточного раунду
    const animate = () => {
        ball.style.left = leftPos + "px";
        ball.style.top = topPos + "px";
        leftPos += stepX;
        topPos += stepY;
    //verify when game is finished
        if( (countRight >= maxCount && (countRight - countLeft >= 2)) ||
            (countLeft >= maxCount && (countLeft - countRight >= 2) )){  
            writingResult(leftPlayerName,countRight,countLeft,rightPlayerName); 
            finishRound(countRight,countLeft); 
            startPosition();
        }        
    //ракетка справа
        if(topPos >= rightRocket.offsetTop - ball.clientHeight / 3 && 
           topPos <= rightRocket.offsetTop + rightRocket.clientHeight + ball.clientHeight / 3 && 
           leftPos  >= rightRocket.offsetLeft - ball.clientWidth ){  
            moveLeft(rightRocket.offsetLeft - ball.clientWidth); //clientLeft
            if(leftPlayer === "computer"){    
                if (height < leftRocket.clientHeight / 2){
                    leftRocketNewOffset = imgField.offsetTop + ball.clientHeight / 2 ;
                }else if(height > imgField.clientHeight - leftRocket.clientHeight){
                    leftRocketNewOffset = imgField.clientHeight - leftRocket.clientHeight - ball.clientHeight / 2 ;
                }else{
                    leftRocketNewOffset = height - leftRocket.clientHeight /2;
                }
                leftRocket.style.top = leftRocketNewOffset + "px";
                leftRocket.style.left = leftRocket.offsetLeft + "px";
            }
        }
    //стінка справа
        if(leftPos >= imgField.clientWidth - ball.clientWidth){
            countLeft += 1;
            divCountLeft.innerText  = countLeft;
            console.log("countLeft", countLeft);
            moveLeft(imgField.clientWidth - ball.clientWidth);
            if(leftPlayer === "computer"){    
                if (height < leftRocket.clientHeight / 2){
                    leftRocketNewOffset = imgField.offsetTop + ball.clientHeight / 2 ;
                }else if(height > imgField.clientHeight - leftRocket.clientHeight){
                    leftRocketNewOffset = imgField.clientHeight - leftRocket.clientHeight - ball.clientHeight / 2 ;
                }else{
                    leftRocketNewOffset = height - leftRocket.clientHeight /2;
                }
                leftRocket.style.top = leftRocketNewOffset + "px";
                leftRocket.style.left = leftRocket.offsetLeft + "px";
            }
        }
    //ракетка зліва
        if(topPos >= leftRocket.offsetTop - ball.clientHeight / 3 && 
            topPos <= leftRocket.offsetTop + leftRocket.clientHeight + ball.clientHeight / 3 && 
            leftPos  <= leftRocket.offsetLeft + leftRocket.clientWidth){  
            moveRight(leftRocket.offsetLeft + leftRocket.clientWidth); //clientLeft
            if(rightPlayer === "computer"){
                if (height < rightRocket.clientHeight / 2){
                    rightRocketNewOffset = imgField.offsetTop + ball.clientHeight / 2 ;
                }else if(height > imgField.clientHeight - rightRocket.clientHeight){
                    rightRocketNewOffset = imgField.clientHeight - rightRocket.clientHeight - ball.clientHeight / 2 ;
                }else{
                    rightRocketNewOffset = height - rightRocket.clientHeight /2;
                }
                rightRocket.style.top = rightRocketNewOffset + "px";
                rightRocket.style.left = rightRocket.offsetLeft + "px";
            }
        }
    //стінка зліва
        if(leftPos <= leftRocket.offsetLeft){
            countRight += 1;
            divCountRight.innerText  = countRight;
            console.log("countRight", countRight);
            moveRight(leftRocket.offsetLeft);
            if(rightPlayer === "computer"){
                if (height < rightRocket.clientHeight / 2){
                    rightRocketNewOffset = imgField.offsetTop + ball.clientHeight / 2 ;
                }else if(height > imgField.clientHeight - rightRocket.clientHeight){
                    rightRocketNewOffset = imgField.clientHeight - rightRocket.clientHeight - ball.clientHeight / 2 ;
                }else{
                    rightRocketNewOffset = height - rightRocket.clientHeight /2;
                }
                rightRocket.style.top = rightRocketNewOffset + "px";
                rightRocket.style.left = rightRocket.offsetLeft + "px";
            }
        }
    } // end of animation

});

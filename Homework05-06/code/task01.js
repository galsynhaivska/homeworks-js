//функція-конструктор класу Human
function Human(name, lastname, age, city){
    this.name = name;
    this.lastname = lastname;
    this.age = age;
    this.city = city;
}
//функція-конструктор класу oneObj 
function oneObj(){
    this.getDate = function(){   // метод для екземплярів класу oneObj 
        return `Сьогодні ${Date()} <br>`
    }
}

//метод класу Human
Human.prototype.show = function() {
    return `Картка користувача ${this.name} <br/>
    ПІБ: ${this.lastname} <br>
    Вік: ${this.age} <br>
    Місто: ${this.city} <br>`;
}

//функція isEmpty, яка повертає true, якщо обєкт не має властивостей
const isEmpty = (obj) => {
    for (let proper in obj){
        return false;
    }
    return true;
}

const user1 = new Human("Іван", "Сніжко", "21", "Київ"); // 4 properties, 1 method
document.write(user1.show());

const user0 = new oneObj(); // no properties, but 1 method
const user2 = new Object(); // no properties
const user3 = new Object(); 
user3.name = '';             // 1 property
const user4 = {};           // no properties

// перевірка, вивід на екран 
document.write(' <br> Чи є масив пустим? <br>')
document.write(`user1 - ${isEmpty(user1)} <br>`);
document.write(`user0 - ${isEmpty(user0)} <br>`);
document.write(`user2 - ${isEmpty(user2)} <br>`);
document.write(`user3 - ${isEmpty(user3)} <br>`);
document.write(`user4 - ${isEmpty(user4)} <br>`);
document.write(user0.getDate());


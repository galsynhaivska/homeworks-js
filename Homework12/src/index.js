window.addEventListener("DOMContentLoaded",()=>{
    const btn = document.querySelector(".keys"),
    display = document.querySelector(".display > input"),
    divMem = document.createElement("div");
    //створюємо div з "m"
    divMem.innerHTML = "m";
    divMem.style.backgroundColor = "#bccd95;";
    divMem.style.textAlign = "left"
    divMem.style.position = "absolute";
    divMem.style.top = `${display.style.top}`;
    divMem.style.left = `${display.style.left}`;
    divMem.style.paddingLeft = "5px"
    divMem.style.zIndex = "2";
    divMem.style.display = "none";    
    display.before(divMem);
    document.getElementById("equal").disabled = false;
    clearInput(display); // в input відображаємо 0

    btn.addEventListener("click", function (e) {
    // клік мишкою на клавішу "C"
        if(e.target.value === "C"){
            clearInput(display); // в input відображаємо 0
            clearCalc(); // число1, число2 та дія  = ""
        }    
    // клік мишкою на клавішу дії "+"", "-", "*" або "/"
        if(e.target.className === "button pink"){
            // якщо число1 ще не було введено            
            if(calc.value1 === "" ){   
                calc.value1 = 0; // то вважаємо, що "число1 = 0"
            }
            // якщо число1 було введено, а число2 ще не було введено    
            if(calc.value1 !== "" && calc.value2 === ""){
                last = e.target.value;   //  то зберігаємо  цю дію  в змінну last     
            }
            // якщо число1 і число2 були введені
            if(calc.value1 !== "" && calc.value2 !== ""){
            // то спочатку виконуємо дію з оператором, збереженим  у змінній last
                value = evaluate(parseFloat(calc.value1), parseFloat(calc.value2), last);
                show(value, display);  // і показуємо результат в input
                last = e.target.value; //після цього зберігаємо нову дію в змінну last
                calc.value1 = value; // i числу1 присвоюємо результат, 
                calc.value2 = "";    // a число2 стає undefined
            }
        }
    // клік мишкою на клавішу з цифрою або "."
        if(e.target.className === "button black" && e.target.value !== "C" ){
        // якщо ще не було введено дію і число2, то вводимо число1    
            if(calc.value2 === "" && last === ""){
                if(e.target.value === "." && calc.value1 === ""){
                    calc.value1 = "0";
                }
                calc.value1 += e.target.value;
                show(calc.value1, display);   // і показуємо його в input
            }
        // якщо число1 і дія вже є, то вводимо число2      
            if(calc.value1 !== "" && last !== "") {
                if(e.target.value === "." && calc.value2 === ""){
                    calc.value2 = "0";
                }
                calc.value2 += e.target.value;
                show(calc.value2, display);   // і показуємо його в input
            }
        }
    // клік мишкою на клавішу "="    
        if(e.target.value === "="){
            // якщо число1 ще не було введено
            if(calc.value1 === ""){
                calc.value1 = "0";
            }
            // якщо число2 ще не було введено
            if(calc.value2 === ""){
                calc.value2 = calc.value1;
                value = calc.value1;
            }
            // якщо число1 і число2  визначені  
            //if(calc.value1 !== "" && calc.value2 !== ""){
            if(last !== ""){    
                // то виконуємо дію з оператором, збереженим  у змінній last
                    value = evaluate(parseFloat(calc.value1), parseFloat(calc.value2), last);
                    show(value, display);  // і показуємо результат в input
                    calc.value1 = value; // i числу1 присвоюємо результат, 
                    calc.value2 = "";    // a число2 стає число1
            }else{
                calc.value2 = "";
                value = calc.value1;
            }
            show(value, display);   // і показуємо його в input
        }
        //клік на m+ додає число з input до числа в пам'яті
        if(e.target.value === "m+"){
            if(calc.mem === ""){
                calc.mem = 0;
            }
            calc.mem = parseFloat(calc.mem ) + parseFloat(display.value);
            if(divMem.style.display = "none"){
                divMem.style.display = "flex";  
            }
            clearCalc();
            console.log(calc.mem);
        }
        //клік на m- віднімає число з input від числа в пам'яті
        if(e.target.value === "m-"){
            if(calc.mem === ""){
                calc.mem = 0;
            }
            calc.mem = parseFloat(calc.mem) - parseFloat(display.value);
            if(divMem.style.display = "none"){
                divMem.style.display = "flex";   
            }
            clearCalc();
            console.log(calc.mem);
        }
        //клік на mrc 
        if(e.target.value === "mrc"){
            countMrc++;
            if(countMrc === 2){          //другий раз  
                divMem.style.display = "none";
                clearCalc();
                countMrc = 0;
                calc.mem = "";
                console.log(calc.mem);
            }else{                          //перший раз
                show(calc.mem, display); //показуємо в input
                if(divMem.style.display = "none"){
                    divMem.style.display = "flex";   
                }
                clearCalc();
                console.log(calc.mem);
            }
        }
    })
})

// відображення в input
function show (value, el) {
//value = parseFloat(value);
    el.value = value;
}
//очистити input
function clearInput(el){
    el.value = "0";
}
//очистити дані
function clearCalc(){
    calc.value1 = "";
    calc.value2 = "";
    last = "";
}
// арифметичні функції    
    const add = (a, b) =>{
        return a + b;
    }
    const div = (a, b) =>{
        if(b === 0) {
            console.error("ПОМИЛКА");
        }
        return a / b;
    }
    const sub = (a, b) =>{
        return a - b;
    }
    const mul = (a, b) =>{    space = " ";
        return a * b;
    }
    function evaluate(a, b, s) { 
        switch(s){                 
        case "+": return add(a,b); 
        case "-": return sub(a,b); 
        case "*": return mul(a,b); 
        case "/": return div(a,b);
        }
    }
const calc = {
    value1 : "",
    value2 : "",
    mem : ""
}
let last = "",   //для зберігання дії 
    value = "0" , // для зберігання результату
    countMrc = 0;  // лічильник для кліків mrc

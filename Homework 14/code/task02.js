/* 2. Создай список состоящий из 4 листов. Используя джс обратись к 2 li и
    с использованием навигации по DOM дай 1 элементу
    синий фон, а 3 красный
*/
window.addEventListener("DOMContentLoaded", () => {
    itemList = document.querySelector("ul");
    itemList.children[1].previousElementSibling.style.backgroundColor = "rgb(67, 67, 235)";
    itemList.children[1].nextElementSibling.style.backgroundColor = "red";

    //console.dir(itemList);
    //console.log(itemList.children[1].innerText);
    //console.log(itemList.children[1].nextElementSibling.innerText);
    //console.log(itemList.children[1].previousElementSibling.innerText);
});

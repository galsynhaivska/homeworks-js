/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага,
водій типу Driver, мотор типу Engine.
Методи start(), stop(), turnRight(), turnLeft(),
які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.
Створити похідний від Car клас - Lorry (вантажівка), 
що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, 
який також характеризується граничною швидкістю.
*/

class Driver{
    constructor(lastName, firstName, middleName, experience){
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.experience = experience;
    }
}
class Engine{
    constructor(motorPower, producer){
        this.motorPower = motorPower;
        this.producer = producer;
    }
}

class Car{
    constructor(carBrand, carClass, carWeight, Driver, Engine){
        this.driver = Driver; 
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.carWeight = carWeight;
        this.engine = Engine;
    }

    start(){
        return "Поїхали";
    }
    stop(){
        return "Зупиняємося";
    }
    turnRight(){
        return "Поворот праворуч";
    }
    turnLeft(){
        return "Поворот ліворуч";
    }
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass}<br> 
        вага ${this.carWeight}<br>
        мотор ${this.engine.motorPower}, виробник ${this.engine.producer} <br>
        водій ${this.driver.lastName} ${this.driver.firstName} ${this.driver.middleName} <br>
        стаж водіння ${this.driver.experience} <br> `
    }
}
class SportCar extends Car {
    constructor(carBrand, carClass, carWeight, Driver, Engine, maxSpeed){
        super(carBrand, carClass, carWeight, Driver, Engine);
        this.maxSpeed = maxSpeed;
    }
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass}<br> 
        вага ${this.carWeight}<br>
        мотор ${this.engine.motorPower}, виробник ${this.engine.producer} <br>
        водій ${this.driver.lastName} ${this.driver.firstName} ${this.driver.middleName} <br>
        стаж водіння ${this.driver.experience} <br> 
        гранична швидкість ${this.maxSpeed} <br>`
    }    
}
class Lorry extends Car {
    constructor(carBrand, carClass, carWeight, Driver, Engine, capacity){
        super(carBrand, carClass, carWeight, Driver, Engine);
        this.capacity = capacity;
    }
    toString(){
        return `<br> 
        Інформація про автомобіль: <br>
        марка автомобіля ${this.carBrand} <br>
        клас автомобіля ${this.carClass}<br> 
        вага ${this.carWeight}<br>
        мотор ${this.engine.motorPower}, виробник ${this.engine.producer} <br>
        водій ${this.driver.lastName} ${this.driver.firstName} ${this.driver.middleName} <br>
        стаж водіння ${this.driver.experience} <br> 
        вантажопідйомність ${this.capacity} <br>`
    }    
}

function CarToString(arrCar) {
    for (let i=0; i < arrCar.length; i++){
        arrCar[i].toString()
    }
}

const drivers = [new Driver("Ivaniv", "Petro", "Marianovych", 7), 
                    new Driver("Petriv", "Mark", "Serhijovych", 2),
                    new Driver("Volyk", "Victor", "Petrovych", 12)];
const cars = [new Car("BMW G20", "D", "1800", drivers[2], new Engine("135 kW", "BMW")),
            new Car("Audi TT", "S", "1500", drivers[1], new Engine("150 kW", "Audi")), 
            new Car("Ford Fiesta", "B", "1150", drivers[2], new Engine("115 kW", "Ford")),
            new Car("Opel Astra", "C", "1400", drivers[0], new Engine("120 kW", "Opel"))]; 
const sportCars = new SportCar("Porsche 911", "S", "1500", drivers[1], 
            new Engine("331 kW", "Porsche"), "306 km/h");
const lorry = new Lorry("Hyundai", "S", "2500", drivers[2], 
            new Engine("103kW", "Hyundai"), "4600 kg");
const arrCar = [cars, sportCars, lorry];
console.log(cars);
console.log(drivers);
console.log(sportCars);
console.log(lorry);
console.log(arrCar);
//console.log(engine);
//document.write(JSON.stringify(cars));
console.log(`${cars[0].carBrand} : ${cars[0].turnLeft()}`);
console.log(`${cars[1].carBrand} : ${cars[0].start()}`);
console.log(`${cars[2].carBrand} : ${cars[0].turnRight()}`);
console.log(`${cars[3].carBrand} : ${cars[0].stop()}`);
console.log(`${cars[0].driver.lastName} : ${cars[0].stop()}`);
document.write(arrCar.join(" ").toString())
//document.write(cars.join(" ").toString());
//document.write(sportCars.toString());
//document.write(lorry.toString());

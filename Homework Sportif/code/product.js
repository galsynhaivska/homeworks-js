import {itemComment} from "./data-comments.js";
import {itemDescription} from "./data-describe.js";
import {colors} from "./data-color.js";
import {setPageName, setSelectGroup} from "./function.js";
import {createCommentCart} from "./product-function.js";

//click on navigation
setPageName();
setSelectGroup();
const spanText = document.querySelector(".select-group-item");
//selectItem - це обраний товар
const selectItem =  JSON.parse(window.sessionStorage.selectItem);
spanText.innerText +=` > ${selectItem[0].itemName}`;
//не забуваємо про дані для userCart !!!
//------------------------------------------
let counterItem;
let userCard =[];
const divCounterCard = document.querySelector(".counter-shopping-cart");
if(!window.localStorage.userCard){
    userCard =[];
    counterItem = 0; 
    
}else{
    userCard = JSON.parse(window.localStorage.userCard);
    counterItem = userCard.length;    
    
}
divCounterCard.innerText = counterItem;
//------------------------------------------

//вибираємо опис товару з 'data-describe.js'
const itemDescribed = itemDescription.filter(elem => elem.idItem === selectItem[0].idItem); 
//console.log(itemDescription);
//console.log(selectItem);
//console.log(selectItem[0].idItem);
//console.log(itemDescribed);
//заповнюємо сторінку  даними обраного товару
// 1 - шукаємо елементи на сторінці -product-
const bigImg = document.querySelector(".big-img"),
    firstFoto = document.querySelector(".first-foto"),
    backFoto = document.querySelector(".second-foto"),
    productTitle = document.querySelector(".product-title"),
    spanProduct = document.querySelector(".span-product-title"),
    infoReviews = document.querySelector(".info-reviews"),
    priceProduct = document.querySelector(".price-product"),
    colorContainer =  document.querySelector(".color-container"),
    sizeContainer =  document.querySelector(".size-container"),
    itemDetails =  document.querySelector(".text-details > p");
const itemProperties =  document.querySelector(".ul-product-properties");
// 2- завантажуємо foto
bigImg.src = `.${selectItem[0].imageFront}`;
firstFoto.src = `.${selectItem[0].imageFront}`;
firstFoto.classList.add("active-foto");
if(selectItem[0].imageBack === ""){
    backFoto.classList.add("hidden-foto");
}else{
    backFoto.classList.remove("hidden-foto");
    backFoto.src = `.${selectItem[0].imageBack}`;
}

// 3 - виводимо назву, id та ціну
productTitle.innerText = selectItem[0].itemName;
spanProduct.innerText = `ITEM #${selectItem[0].idItem}`;
priceProduct.innerText = `$${selectItem[0].price}`;

// 4 - виводимо опис товару з itemDescription в розділ 'Details'
if(itemDescribed.length !== 0){   
itemDetails.innerText = itemDescribed[0].descripton;
for(let i = 0; i < itemDescribed[0].details.length; i++){
  const  liProp = document.createElement("li");
  liProp.innerText = itemDescribed[0].details[i];
  itemProperties.append(liProp);
}
}else{
    itemDetails.innerText = "No details"; 
}

// 5 - заповнюємо контейнер colorContainer кольорами товару 
// 5-1 групуємо дані в selectItem по кольорам
let arr1 = selectItem.reduce((r, i) => {
    r[i.color] = r[i.color] || [];
    r[i.color].push(i);
    return r;
}, {});
let arrColors = [];
for(let key in arr1) {
    arrColors.push(arr1[key]);
}
//console.log("arrColors=", arrColors)

// 5-2 додаємо в контейнер div з кольорами
for(let i = 0; i < arrColors.length; i++){
    colors.forEach(el =>{
        if(arrColors[i][0].color === el.colorName){   
            let divColor = document.createElement("div");
            divColor.classList.add("color-container-div");
            divColor.style.backgroundColor =`rgba(${el.rgba})`;
            colorContainer.append(divColor);
            divColor.dataset.clicked = false;
        }
    });    
}

// 5-3 записуємо назву кольору в -span-color-
const selectColor = document.querySelector(".color-container");
const firstItem = selectColor.children[0];
firstItem.dataset.clicked = true;
firstItem.classList.add("active-color");
let mainColor = "";
colors.forEach(el =>{
if(firstItem.style.backgroundColor.includes(el.rgba)){
    mainColor = el.colorName;
    document.querySelector(".span-color").innerText = ` ${el.colorName}`;
}
});

// 6 - заповнюємо контейнер -sizeContainer- розмірами для обраного кольору
for(let i = 0; i < arrColors.length; i++){
    if(arrColors[i][0].color === mainColor){
        for(let j = 0; j < arrColors[i].length; j++){
            if(parseFloat(arrColors[i][j].quantity) !== 0){
                let divSize = document.createElement("div");
                divSize.classList.add("size-container-div");
                divSize.innerText = arrColors[i][j].sizeItem;
                sizeContainer.append(divSize);
                divSize.dataset.clicked = false;
            }
        }
    }
}
//----------------------------------------------------
//click on colors and sizes
const [...itemColors] = document.querySelectorAll(".color-container-div");
//console.log(itemColors);
let [...itemSizes] = document.querySelectorAll(".size-container-div");
//console.log(arrColors);
//click on fotos
firstFoto.addEventListener("click", (e)=>{
    const divActive = document.querySelector(".color-container-div[data-clicked = true]");
    let index = itemColors.findIndex(item => item.style.backgroundColor === divActive.style.backgroundColor);
    bigImg.src = `.${arrColors[index][0].imageFront}`;
    firstFoto.classList.add("active-foto");
    backFoto.classList.remove("active-foto");
});
backFoto.addEventListener("click", (e)=>{
    const divActive = document.querySelector(".color-container-div[data-clicked = true]");
    let index = itemColors.findIndex(item => item.style.backgroundColor === divActive.style.backgroundColor);
    bigImg.src = `.${arrColors[index][0].imageBack}`;
    backFoto.classList.add("active-foto");
    firstFoto.classList.remove("active-foto");
});
//click on colors 
let selectColorItem = "";
itemColors.forEach(elem => {
    elem.addEventListener("click", (ev) =>{ 
        const clickColor = document.querySelector('.color-container [data-clicked="true"]');  
        if( clickColor){
            clickColor.classList.remove("active-color");
            clickColor.dataset.clicked = false;
        }
        ev.target.dataset.clicked = true;
        ev.target.classList.add("active-color");
        colors.forEach(el =>{
            if(ev.target.style.backgroundColor.includes(el.rgba)){
                selectColorItem = el.colorName;
            //    console.log(selectColorItem);
                document.querySelector(".span-color").innerText = ` ${el.colorName}`;
            }
            for(let i = 0; i < arrColors.length; i++){
                if(arrColors[i][0].color ===  selectColorItem){
                    bigImg.src = `.${arrColors[i][0].imageFront}`;
                    firstFoto.src = `.${arrColors[i][0].imageFront}`;
                    backFoto.src = `.${arrColors[i][0].imageBack}`;
                }   
            }
        });
    //fill size-container    
        sizeContainer.innerHTML = "";    
        for(let i = 0; i < arrColors.length; i++){
            if(arrColors[i][0].color === selectColorItem){
                for(let j = 0; j < arrColors[i].length; j++){
                    if(parseFloat(arrColors[i][j].quantity) !== 0){
                        let divSize = document.createElement("div");
                        divSize.classList.add("size-container-div");
                        divSize.innerText = arrColors[i][j].sizeItem;
                        sizeContainer.append(divSize);
                        divSize.dataset.clicked = false;
                    }
                }
            }
        }
        [...itemSizes] = document.querySelectorAll(".size-container-div");
        itemSizes.forEach(elem => {
            elem.addEventListener("click", (ev) =>{
                const clickSize = document.querySelector('.size-container [data-clicked="true"]');
                if(clickSize){
                    clickSize.classList.remove("active-size");
                    clickSize.dataset.clicked = false;
                }
                ev.target.dataset.clicked = true;
                ev.target.classList.add("active-size");
                document.querySelector(".span-size").innerText = ` ${ev.target.innerText}`;
            });
        });
    });
});
//----------------------------------------------------------
//click on sizes
[...itemSizes] = document.querySelectorAll(".size-container-div");
itemSizes.forEach(elem => {
    elem.addEventListener("click", (ev) =>{
        const clickSize = document.querySelector('.size-container [data-clicked="true"]');
        if(clickSize){
            clickSize.classList.remove("active-size");
            clickSize.dataset.clicked = false;
        }
        ev.target.dataset.clicked = true;
        ev.target.classList.add("active-size");
        document.querySelector(".span-size").innerText = ` ${ev.target.innerText}`;
    });
});
//--------------------------------------------------------------
//click on "ADD TO BAG"
const addToCard = document.querySelector(".btn-bag");
//console.log(addToCard);
addToCard.addEventListener("click", () => {
    let sizeSelect = "",  colorSelect = "";
    const clickSize = document.querySelector('.size-container [data-clicked="true"]');    
    if(clickSize){
        sizeSelect = clickSize.innerText;
        itemColors.forEach(el =>{
            const clickColor = document.querySelector('.color-container [data-clicked="true"]');
            if( clickColor){
                colors.forEach(el =>{
                    if(clickColor.style.backgroundColor.includes(el.rgba)){ 
                        colorSelect = el.colorName; 
                    }
                });
            }  
        }); 
        for(let i = 0; i < selectItem.length; i++){
            if(selectItem[i].sizeItem === sizeSelect && selectItem[i].color === colorSelect){
                console.log( selectItem[i]);
                if(userCard.some(item => item.idItem === selectItem[i].idItem 
                    && item.color === selectItem[i].color 
                    && item.sizeItem === selectItem[i].sizeItem)){
                const index = userCard.findIndex(item => item.idItem === selectItem[i].idItem 
                    && item.color === selectItem[i].color 
                    && item.sizeItem === selectItem[i].sizeItem);
    //                    console.log(index);  
                userCard[index].quantity =  parseInt(userCard[index].quantity) + 1;
    //                    console.log( userCard[index].quantity);
                }else{    
                    userCard.push(selectItem[i]);
                    userCard[userCard.length - 1].quantity = 1;
                }
                window.localStorage.userCard = JSON.stringify(userCard); 
                counterItem = userCard.length;
                divCounterCard.innerText = counterItem;    
            }
        }
    }else{
        alert("Choose size");
    }    
}); 
//-----------------------------------------------------------

// 7 - заповнюємо -reviewContainer-
const nextPage = document.querySelector(".comment-next-page");
const [...btnNextPage] = document.querySelectorAll(".btn-page.comment");
const commentaries = itemComment.filter(elem => elem.itemId === selectItem[0].idItem); 

document.querySelector(".info-reviews").innerText = `${commentaries.length} reviews`
//window.sessionStorage.commentaries = JSON.stringify(commentaries);

const numberComment = 6; //виводимо по 6 коментарів на сторінку
const maxPage = Math.ceil(commentaries.length / numberComment);
if(commentaries.length === 0){
    document.querySelector(".no-reviews").style.display = "block";
    nextPage.style.display = "none";
}else if(commentaries.length > numberComment){   
    for(let i = 0; i < numberComment; i++){
        createCommentCart(commentaries[i]);   
        btnNextPage[0].classList.add("btn-page-active");
        btnNextPage[0].dataset.pageNumber = 1;
    }
}else{
    for(let i = 0; i < commentaries.length; i++){
        createCommentCart(commentaries[i]);    
        nextPage.style.display = "none"; 
    } 
}

//7-2 перехід між сторінками в Reviews
let page = 0;
btnNextPage.forEach(elem =>{
    elem.addEventListener("click", (ev) =>{
        const commentPage = [];
        console.log(document.querySelector(".btn-page-active"))
        document.querySelector(".btn-page-active").classList.remove("btn-page-active");
        document.querySelector(".container-reviews").innerText = "";
        elem.classList.add("btn-page-active");
        
        if(ev.target.innerText === ">"){
            page = maxPage;
        }else{
            page = ev.target.innerText;
        }
        if(commentaries.length > numberComment * page){
            for(let i = (page - 1)* numberComment; i <  numberComment * page; i++){
                commentPage.push(commentaries[i]);
                createCommentCart(commentaries[i]); 
            }
        }else{
            for(let i = (page - 1)* numberComment; i <  commentaries.length; i++){
                commentPage.push(commentaries[i]);
                createCommentCart(commentaries[i]); 
            }
        }
         elem.classList.add("btn-page-active");
    });    
});
//fill item-title in user-review
const itemNameTitle = document.querySelector(".item-name-title");
itemNameTitle.innerText = selectItem[0].itemName;





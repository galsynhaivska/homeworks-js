let styles = ["Джаз", "Блюз"];
alert(`Ви створили масив: \n  ${styles.join(", ")}`);

// 1 - add "Rock-n-roll" to end of stales
const a2 = "Рок-н-рол";
styles.push(a2);
alert(`Ви додали елемент "${a2}" в кінець масиву \n
  і отримали масив: \n  ${styles.join(", ")}`); 

//2 - find mediana of array
let pos = Math.floor(styles.length / 2);    //console.log(pos);
// and use splice for replace of mediana
const a3 = styles.splice(pos, 1, "Класика");
alert(`Ви замінили елемент  "${a3}" в середині масиву 
на "Класика" \n 
  i отримали масив: \n  ${styles.join(", ")}`);

//3 - remove first element and show it
alert(`Ви видалили з масиву перший елемент: \n ${styles.shift()} \n
  i отримали масив: \n ${styles.join(", ")}`);


//4 - add two elements into the begin of styles
styles.unshift("Реп", "Реггі");
alert(`Ви додали елементи "Реп" та "Реггі" на початок масиву \n
   і отримали масив: \n  ${styles.join(", ")}`);


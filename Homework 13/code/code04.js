/* 4. При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
Це поле буде служити для введення числових значень
Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. 
- При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, 
над полем створюється `span`, в якому має бути виведений текст: 
 Поруч із ним має бути кнопка з хрестиком (`X`). 
Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 
    - при втраті фокусу підсвічувати поле введення червоною рамкою, 
    - під полем виводити фразу - `Please enter correct price`. 
    -`span` зі значенням при цьому не створюється.
*/

window.addEventListener("DOMContentLoaded", () => {
    const keysNumber = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-'],
        input = document.querySelector('input'),
        divErr = document.createElement("div");
    let isKey = false;    
 
    divErr.innerText = "Please enter correct price";
    
    input.addEventListener("keypress", function(e) {
        isKey = false;
        for (let key of keysNumber) {
            if(key === e.key){
                isKey = true;
            };    
        };    
        if(isKey === false){
            e.preventDefault();    
        };
    });   
    
    input.addEventListener("focus", function(e) {
        e.target.style.border = "solid 5px green";
        e.target.style.backgroundColor = "";
    });
    
    input.addEventListener("blur", function(e) {
        console.log(e.target.value[0])
        if(parseFloat(e.target.value) < 0 || parseFloat(e.target.value) / e.target.value !== 1 || e.target.value[0] == 0) {
            input.parentElement.append(divErr);
            e.target.style.border = "solid 5px red";
        }else{    
            e.target.style.border = "";
            e.target.style.backgroundColor = "green";
            const text = e.target.value;
            //create div with span and button
            const div = document.createElement("div"),
                span = document.createElement("span"),
                btnX = document.createElement("button");
            div.classList.add("price");
            span.innerText = e.target.value;
            span.style.padding = "10px";
            btnX.innerText = 'X';
            // add div before input
            e.target.parentElement.before(div);
            div.append(span); //add span in div
            div.append(btnX); //add button in div
            if(divErr){
                divErr.remove(); //remove div with Error message if it exists
            }    
        }
    const [...divs] = document.querySelectorAll(".price"); //collect all divs in collection
        divs.forEach(item =>{
            item.addEventListener("click", () => {
                item.remove();
            });
        });
    });
});

//* Клас, об'єкти якого описують параметри гамбургера.
//    class Hamburger {
//        constructor(size, stuffing){
//            this.size = size;
//            this.stuffing = stuffing;
//            this.topping = [];
//        }
//   }

    class HamburgerException extends Error {
        constructor(message){
        super(message);
        this.name = 'HamburgerException';
        }    
    }

    function Hamburger (size, stuffing){
        if(!size && !stuffing){
            throw new HamburgerException("Size and stuffing are not given");               
        }  
        if(!size || !stuffing){
            throw new HamburgerException("Size or stuffing is not given");               
        } 
        if(!parameters.sizes.includes(size)){
            throw new HamburgerException(`Invalid size "${size.name}"`);               
        } 
       
        if(!parameters.stuffings.includes(stuffing)){
            throw new HamburgerException(`Invalid staffing "${stuffing.name}"`);               
        } 
        this.size = size;
        this.stuffing = stuffing;
        this.topping = [];
    }

//Дізнатися розмір гамбургера
    Hamburger.prototype.getSize = function() {
        return this.size.name;
    }  
//Дізнатися, яка начинка у гамбургера
    Hamburger.prototype.getStuffing = function() {
        return this.stuffing.name;
    } 
// Отримати список добавок
    Hamburger.prototype.getTopping = function() {
        let toppingStr = [];
            if(this.topping.length !== 0){
            this.topping.forEach((el) =>{
                toppingStr += ` ${el.name}`;
            });        
            return toppingStr 
        }else{
            return "Без добавок";
        }   
    } 

//Змінити начинку гамбургера
    Hamburger.prototype.changeStuffing = function(stuffing){
        if(!parameters.stuffings.includes(stuffing)){
            throw new HamburgerException(`Invalid staffing "${stuffing.name}"`);               
        } 
        return this.stuffing = stuffing;
        }   
//Додати добавку
    Hamburger.prototype.addTopping = function(topping){
        if(!parameters.toppings.includes(topping)){
            throw new HamburgerException(`Invalid topping "${topping.name}"`);               
        } 
        if(this.topping.length === 0){
            this.topping.push(topping);
            return topping.name;
        }else{
            if(this.topping.find(item => item.name === topping.name)){
                throw new HamburgerException(`Duplicate topping "${topping.name}"`);        
            }else{
                this.topping.push(topping);
                return topping.name;
            }
       }
    }   
//Прибрати добавку 
    Hamburger.prototype.removeTopping = function (topping){
        if(!parameters.toppings.includes(topping)){
            throw new HamburgerException(`Invalid topping "${topping.name}"`);               
        } 
        if(this.topping.length !== 0){
            if(this.topping.findIndex(item => item.name === topping.name) !== -1){
                this.topping.splice(this.topping.findIndex(item => item.name === topping.name), 1)
            }    
        return topping.name;
        }
    }
    
//Дізнатись ціну гамбургера
    Hamburger.prototype.calculatePrice = function(){
        let priceTotal = parseInt(this.size.price) +  parseInt(this.stuffing.price);
        if(this.topping.length !== 0){
            this.topping.forEach((el) =>{
                priceTotal += parseInt(el.price);
            })
        }
        return priceTotal;
    }
//Дізнатись калорійність гамбургера     
    Hamburger.prototype.calculateCalories = function(){
        let caloriesTotal = parseInt(this.size.calories) +  parseInt(this.stuffing.calories);
        if(this.topping.length !== 0){
            this.topping.forEach((el) =>{
                caloriesTotal += parseInt(el.calories);
            })
        } 
        return caloriesTotal;
    }    
 
//hamburger's sizes, stuffings and  toppings
    const parameters = {
        sizes: [
            SIZE_SMALL = {name: "small", price: "50", calories: "20"},
            SIZE_LARGE = {name: "large", price: "100", calories: "40"},
        ],
        stuffings: [
            STUFFING_CHEESE = { name: "cheese", price: "10", calories: "20"},
            STUFFING_POTATO = { name: "potato", price: "20", calories: "5"},
            STUFFING_SALAD = { name: "salad", price: "15", calories: "10"},
        ],
        toppings: [
             TOPPING_MAYO = {name: "mayo", price: "20", calories: "5"},
             TOPPING_SPICE = {name: "spice", price: "15", calories: "0"},
        ],
    };   
    console.log(parameters);
    console.log(parameters.stuffings[0]);
//Нижче код для перевірки класу  
try{ 
    const hamburger1 = new Hamburger(SIZE_LARGE, STUFFING_POTATO);   
    console.log(hamburger1);  
    console.log("Розмір гамбургера: ", hamburger1.getSize());
    console.log("Начинка гамбургера: ", hamburger1.getStuffing()); 
    console.log("Добавки: ", hamburger1.getTopping());
    console.log("Калорійність: ", hamburger1.calculateCalories());
    console.log("Ціна гамбургера: ", hamburger1.calculatePrice());
    console.log("Заміна начинки на:", hamburger1.changeStuffing(STUFFING_SALAD).name);
    console.log("Калорійність: ", hamburger1.calculateCalories());
    console.log("Ціна гамбургера: ", hamburger1.calculatePrice());
  
   console.log("Додано добавку: ", hamburger1.addTopping(TOPPING_SPICE));
   console.log("Добавки: ", hamburger1.getTopping());
   console.log("Калорійність: ", hamburger1.calculateCalories());
   console.log("Ціна гамбургера: ", hamburger1.calculatePrice());

   console.log("Додано добавку: ", hamburger1.addTopping(TOPPING_SPICE));
   console.log("Добавки: ", hamburger1.getTopping());
   console.log("Калорійність: ", hamburger1.calculateCalories());
   console.log("Ціна гамбургера: ", hamburger1.calculatePrice());

   console.log("Додано добавку: ", hamburger1.addTopping(TOPPING_MAYO));
   console.log("Добавки: ", hamburger1.getTopping());
   console.log("Калорійність: ", hamburger1.calculateCalories());
   console.log("Ціна гамбургера: ", hamburger1.calculatePrice());

   console.log("Видалено добавку: ", hamburger1.removeTopping(TOPPING_SPICE));
   console.log("Добавки: ", hamburger1.getTopping());
   console.log("Калорійність: ", hamburger1.calculateCalories());
   console.log("Ціна гамбургера: ", hamburger1.calculatePrice());

   console.log("Додано добавку: ", hamburger1.addTopping(TOPPING_MAYO));
   console.log("Добавки: ", hamburger1.getTopping());
   console.log("Калорійність: ", hamburger1.calculateCalories());
   console.log("Ціна гамбургера: ", hamburger1.calculatePrice());

   console.log("Додано добавку: ", hamburger1.addTopping(TOPPING_SPICE));
   console.log("Добавки: ", hamburger1.getTopping());
   console.log("Калорійність: ", hamburger1.calculateCalories());
   console.log("Ціна гамбургера: ", hamburger1.calculatePrice());

   const hamburger2 = new Hamburger(SIZE_SMALL,);   
   
}catch(err){
    if(err.name === "HamburgerException"){
        console.log(err.message);
    }else{
        throw err;
    } 
}
//
try{
    const hamburger3 = new Hamburger(); 
}catch(err){
    if(err.name === "HamburgerException"){ 
       console.log(err.message);
   }else{
    throw err;
    } 
}
try{
    const hamburger3 = new Hamburger(SIZE_SMALL, STUFFING_SALAD);
    console.log("Додано добавку: ", hamburger3.addTopping(TOPPING_MAYO));
    console.log("Калорійність: ", hamburger3.calculateCalories());
    console.log("Ціна гамбургера: ", hamburger3.calculatePrice()); 
    console.log("Заміна начинки на:", hamburger3.changeStuffing(STUFFING_POTATO).name);
    console.log("Калорійність: ", hamburger3.calculateCalories());
    console.log("Ціна гамбургера: ", hamburger3.calculatePrice()); 
    console.log(hamburger3); 
}catch(err){
    if(err.name === "HamburgerException"){ 
       console.log(err.message);
   }else{
    throw err;
    } 
}

try{
    const hamburger4 = new Hamburger(SIZE_SMALL, STUFFING_POTATO);
    console.log("Заміна начинки на:", hamburger4.changeStuffing(TOPPING_SPICE).name);
    console.log("Калорійність: ", hamburger4.calculateCalories());
    console.log("Ціна гамбургера: ", hamburger4.calculatePrice()); 
    console.log(hamburger4); 
}catch(err){
    if(err.name === "HamburgerException"){ 
       console.log(err.message);
   }else{
    throw err;
    } 
}
try{
    const hamburger5 = new Hamburger(SIZE_SMALL, STUFFING_POTATO);
    console.log("Додано добавку: ", hamburger5.addTopping(STUFFING_POTATO));
    console.log("Добавки: ", hamburger5.getTopping());
}catch(err){
    if(err.name === "HamburgerException"){ 
       console.log(err.message);
   }else{
    throw err;
    } 
}
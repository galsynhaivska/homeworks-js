/* Напиши функцiю map(array, fn), яка приймає функцію і масив, 
обробляє кожний елемент масиву цією функцією, і повертає новий масив */

//functions
function map(newArr, callback) {
    return callback(newArr);
}
const summa = (newArr) => {
    for (let i of newArr) {
        newArr[i] += 5;
    }
}
const substr = (newArr) => {
    for (let i of newArr) {
        newArr[i] -= 5;
    }
}
const multiply = (newArr) => {
    for (let i of newArr) {
        newArr[i] *= 5;
    }
}
const divide = (newArr) => {
    for (let i of newArr) {
        newArr[i] /= 5;
    }
}
//program
const len = prompt("Задайте кількість елементів у масиві \n (ціле число від 1 до 100): ", 10);
if (len === null){
    alert("Масив не створено!");
}else if( parseFloat(len) - parseInt(len) != 0 || parseInt(len) === 0 || len === undefined){
    alert("Це не ціле число від 1 до 100!");
    document.getElementById("init").innerHTML += ` <p>Помилка! <br/> Неправильно вказано ціле число! </p>`;
}else if(parseInt(len) > 100){
    alert("Дуже велике число ( > 100)!");
    document.getElementById("init").innerHTML += ` <p>Помилка! <br/> Вказано дуже велике ціле число (> 100) ! </p>`;
}else{
    const numArr = [];
    for (let i = 1; i <= len; i++){
    numArr[i-1] = i-1;
    }
    document.getElementById("init").innerHTML += `${numArr.join(", ")} `;
   
   
    const action = prompt("Вкажіть дію ( + - * /): ", "+");
    if (action == "+" || action == "-" || action == "*" || action == "/"){
        const newArr = numArr.concat();
        switch(action) {
            case "+" : map(newArr, summa);
            break;
            case "-" : map(newArr, substr);
            break;
            case "*" : map(newArr, multiply);
            break;
            case "/" : map(newArr, divide);
            break;
        }
        document.getElementById("res").innerHTML += `${newArr.join(", ")} `;
        document.getElementById("init").innerHTML += `<p>After operation initial array is: ${numArr.join(", ")} <p>`;
   
    }else{
        alert("Ви неправильно вказали дію!");
        document.getElementById("res").innerHTML += ` <p>Помилка! <br/> Неправильно вказана дія! </p>`;
    }
    
}

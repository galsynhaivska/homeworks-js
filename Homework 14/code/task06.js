/* 6. Создай поле для ввода цвета, 
когда пользователь выберет какой-то цвет,
сделай его фоном body
*/
window.addEventListener("DOMContentLoaded", () => {
    const body = document.querySelector("body"),
        input = document.querySelector("#colors"),
        colors = document.querySelector("#colors-list");
        
    // створюємо змінну-масив для кольорів hsl     
    const color = [];
    for(let i = 0; i <= 360; i++){
        color[i] = `hsl(${i}, 100%, 50%)`;
    }

    color.forEach(item =>{
        const option = document.createElement("option");
        option.value = item;
        colors.append(option);  
    })

    input.addEventListener("change", (e) => {
        body.style.backgroundColor = e.target.value;
    });
});
//завантаження сторінки конструктора
window.addEventListener("DOMContentLoaded", function () {
    const price = document.querySelector("div .price > p"), //<div class = "price"><p></p>ЦІНА:
        priceSpan = document.createElement("span"), // span for price
        sauceDivs = document.createElement("div"),   // div for sauces
        topingDivs = document.createElement("div"),  // div for topings
    sauces =  document.querySelector("div .sauces > p"), //<div class = "sauces"><p></p>СОУСИ:
    toping =  document.querySelector("div .topings > p"), //<div class = "sauces"><p></p>ТОПІНГИ
    [...inputRadio] = document.querySelectorAll(".radioIn"); //input class= "radioIn" - 3 elements
//змінні для форми-підтвердження в footer - input, input.button  
  //  const sub = document.querySelector("sub");
    const [...submit] = document.querySelectorAll(".grid > input"),
    divName = document.createElement("p");


    price.append(priceSpan);   //  додаємо span for price після <div class = "price"><p></p>ЦІНА:
    sauces.append(sauceDivs);  //  додаємо div for sauces після <div class = "sauces"><p></p>СОУСИ:
    toping.append(topingDivs); //  додаємо div for topings  після <div class = "topings"><p></p>ТОПІНГИ:
    //додаємо класи для пошуку цих елементів
    sauceDivs.className = 'selectedSauces';  
    topingDivs.className = 'selectedTopings';

    //функція для створення div-sauces and topings
    function createElem(nameEl, txt) {
        const elem = document.createElement(nameEl);
        elem.className = 'div' + txt;
        return elem;
    }
    // функція sizeKorz визначає price за корж відповідно до обраного розміру піци
    function sizeKorz(elem){
        if (elem.value === "small"){
            return pizzaPrice.small;  
        }else if(elem.value === "mid"){
            return pizzaPrice.mid;
        }else{
            return pizzaPrice.big;
        }    
    }  
    //одразу після завантаження контенту сторінки    
    //визначаємо, у якого inputRadio елемента checked = true, 
    //і відповідно до цього записуємо ціну  в priceSpan 
    // price.innerText = "ЦІНА:"
    inputRadio.forEach( function(elem) {  // elem === input with value = small, mid or big
        if(elem.checked) {    
            sumSize = sizeKorz(elem);
            sum = sumSize + sumSauce + sumToping;
            priceSpan.innerText = " " + String(sum)+'грн.';
        }
    }); 
    //вішаємо подію на <label class = "radio">
    [...document.querySelectorAll(".radio")].map((item) => {
        item.onclick = function() {
            sumSize = sizeKorz(item.childNodes[0]);
            sum = sumSize + sumSauce + sumToping;
            priceSpan.innerText = " " + String(sum) +'грн.';
        }
    });
    // подія drag-and-drop на тегах <div class="ingridients"><div><img  class = "draggable"> 
    //визначаємо куди будемо перетягувати - <img src="Pizza_pictures/klassicheskij-bortik_1556622914630.png" alt="Корж класичний">
    let target = document.querySelector("div .table");
    //визначаємо що будемо перетягувати ("div .ingridients > div > img")\
    let [...sources] = document.querySelectorAll(".ingridients > div > img")  //(".draggable");
    sources.forEach(function(source) {
        source.addEventListener('dragstart', function (evt) { // начало операции drag
            this.style.border = "3px dotted #000"; // меняем стиль в начале операции drag & drop
            evt.dataTransfer.effectAllowed = "move";
            evt.dataTransfer.setData("Text", this.id);
            idToping = this.id;
            textToping = this.nextElementSibling.innerText;
        }, false);
    // конец операции drag
        source.addEventListener("dragend", function (evt) {
            this.style.border = ""; // удаляем стили добавленные в начале операции drag & drop 
        }, false);
    });   
    // перетаскиваемый объект попадает в область целевого элемента
    target.addEventListener("dragenter", function (evt) {
        this.style.border = "3px solid red";
    }, false);
    // перетаскиваемый элемент покидает область целевого элемента
    target.addEventListener("dragleave", function (evt) {
        this.style.border = "";
    }, false);
    // отменяем стандартное обработчик события dragover.
    target.addEventListener("dragover", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
            return false;
    }, false);
    // перетаскиваемый элемент отпущен над целевым элементом
    target.addEventListener("drop", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        this.style.border = "";
        // получаем информации которая передавалась через операцию drag & drop 
        let id = evt.dataTransfer.getData("Text"); 
        let elem = document.getElementById(id);
        // добавляем элемент в целевой элемент. Так как в DOM не может быть два идентичных элемента - 
        // элемент удаляется со своей старой позиции.
        const img = document.createElement("img");
        img.src = elem.src;
        img.id = idToping;
        this.appendChild(img);
        // додаємо div > span button               
            if(idToping === "sauceClassic" || idToping === "sauceBBQ" || idToping === "sauceRikotta"){
                divSauce = createElem("div", 'sauce');
                divSauce.value = idToping;
                spanSauce = document.createElement("span");
                spanSauce.innerText = textToping;  // text
                xBtn = document.createElement("button");
                xBtn.classList.add('x');
                xBtn.innerText = "X";
                divSauce.append(spanSauce);
                divSauce.append(xBtn);
                sauceDivs.append(divSauce);
                for(let key in saucePrice){
                    if(key === idToping){
                        value = saucePrice[key];
                    }
                }
                sumSauce += value;
                sum = sumSize + sumSauce + sumToping;
                priceSpan.innerText = " " + String(sum) +'грн.';
            }else{
                divToping = createElem("div", 'top');
                divToping.value = idToping;
                spanToping = document.createElement("span");
                spanToping.innerText = textToping;  // text
                xBtn = document.createElement("button");
                xBtn.classList.add('x');
                xBtn.innerText = "X";
                divToping.append(spanToping);
                divToping.append(xBtn);
                topingDivs.append(divToping);
                for(let key in topingPrice){
                    if(key === idToping){
                        value = topingPrice[key];
                    }
                }
                sumToping += value;
                sum = sumSize + sumSauce + sumToping;
                priceSpan.innerText = " " + String(sum) +'грн.';
            }
            //збираємо колекції соусів і топінгів
            let [...arrToping] = document.querySelectorAll(".selectedTopings > div");
            let [...arrSauce] = document.querySelectorAll(".selectedSauces > div");
            // подія - клік по  div з колекції arrSauce видаляє його      
            arrSauce.forEach(function(item) {
                item.onclick = () => {   
                    idToping = item.value;  
                    document.getElementById(idToping).remove(); //видаляє image з id = idToping
                    for(let key in saucePrice){
                        if(key === idToping){
                            value = saucePrice[key];
                        }
                    }
                    item.remove(); 
                    sumSauce -= value;
                    
                    sum = sumSize + sumSauce + sumToping;
                    priceSpan.innerText = " " + String(sum) +'грн.';
                }
            });   
            // подія - клік по  div з колекції arrToping видаляє його        
            arrToping.forEach(function(item) {
                item.onclick = () => {     
                    idToping = item.value;  
                    document.getElementById(idToping).remove(); //видаляє image з id = idToping          
                    for(let key in topingPrice){
                        if(key === idToping){
                            value = topingPrice[key];
                        }
                    }
                    item.remove();       
                    sumToping -= value;
                    sum = sumSize + sumSauce + sumToping;
                    priceSpan.innerText = " " + String(sum) +'грн.';
                }
            });
            return false;
        }, false);
    //  заповнення і відправка замовлення 
    submit.forEach(function(item) {
        item.addEventListener("click", function(e){
            if(item.name === "cancel"){
               for(i = 0; i <= 2; i++){
                   submit[i].value = ""; 
                   submit[i].style.border = "none";
                } 
                divName.remove();
            }else if (item.name === "btnSubmit"){
                // перевірка даних в input
                for(i = 0; i <= 2; i++){
                    if(!Object.values(regExpr)[i].test(submit[i].value )){
                        submit[i].style.border = "2px solid red";
                        correctData = false;
                    }
                }
                if(correctData){
                    // переходимо на нову сторінку  
                    window.location.assign("./thank-you.html"); 
                }else{
                    //додаємо повідомлення про помилку; в styles.css змінено  "grid-column: 2/3;" на "grid-column: 1/3;"
                    document.querySelector("footer > form > div").append(divName);
                    divName.innerText = "!Щоб оформити замовлення, вкажіть коректні дані!";  
                }
            }else{
                correctData = true;
             //   item.style.border = "none";
                item.style.border = "2px green solid";
            }
        });
    });
/*slider
    let i = 0, showSlide;
    const divSlider = document.querySelector(".slider");
    const imgSlide = document.querySelector(".slider img");
    console.log(imgSlide);
    imgSlide.classList.add("img-slider");
    imgSlide.src = arrImage[0];
    showSlide = setInterval(() => {
        if(i >= arrImage.length){
            i = 0; 
        }
        imgSlide.src = arrImage[i];
      
        i++;
        console.log(i, imgSlide.src);
    }, 3000);
*/   
}); 

// for slider
/*const arrImage = [
    "./slider-images/pizza-1.png",
    "./slider-images/pizza-2.png",
    "./slider-images/pizza-3.png",
    "./slider-images/pizza-4.png",
    "./slider-images/pizza-5.png",
    "./slider-images/pizza-6.png",
    "./slider-images/pizza-7.png",
    "./slider-images/pizza-8.png",
    "./slider-images/pizza-9.png",
    "./slider-images/pizza-10.png",
];
*/

//
const pizzaPrice = {small: 220, mid: 250, big: 270}, // price за корж відповідно до розміру піци 
saucePrice = {sauceClassic: 15, sauceBBQ: 20, sauceRikotta: 25},
topingPrice = {moc1: 25, moc2: 30, moc3: 35, telya: 35, vetch1: 15, vetch2: 25},
regExpr = {name: /\b[A-Za-z']{3,17}\b/, phone: /[+]\d{12}/, email: /\b[A-z0-9._-]+@[a-z0-9-.]+.[a-z]{2,3}\b/};
let sum = 0, sumToping = 0, sumSauce = 0, sumSize = 0,
    idToping, textToping, 
    source, correctData = true;

// функція обчислює нові координати банера "Отримай знижку"
function random(min, max, size) {
    const rand = min + Math.random() *(max - min);
    if (rand + size > max){
         return rand - size
    }else{
        return rand;
    }


}
// банер "Отримай знижку"
const banner = document.querySelector("#banner");
const width = banner.clientWidth;
const height = banner.clientHeight;

banner.addEventListener('mouseover', () => {
    banner.style.left = random(0, window.innerWidth*0.9, width) + 'px';
    banner.style.top = random(0, window.innerHeight*0.9, height) + 'px';
    banner.style.width = width + 'px';
    banner.style.height = height +'px';
})
     
   
   


  

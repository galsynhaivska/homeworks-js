/*5. Создай див и сделай так, чтобы при наведении на див, 
див изменял свое положение на странице
*/
window.addEventListener("DOMContentLoaded", () => {
    const divMove = document.createElement("div"),
        divText = document.createElement("div"),
        divForm = document.querySelector(".form-name");

    divMove.classList.add("div-move");
    divText.classList.add("div-text");
    divText.innerText = "Click me!";
    divForm.append(divMove);
    divMove.append(divText);

    divMove.addEventListener("mouseover", (e) =>{
        let x = Math.random() * (window.innerWidth - divMove.offsetWidth);
        let y = Math.random() * (window.innerHeight - divMove.offsetHeight);
        e.target.style.left = x +"px";
        e.target.style.top = y +"px";
    //    console.log(x, y);  
    //    console.log(window.innerWidth, window.innerHeight);
    });
});

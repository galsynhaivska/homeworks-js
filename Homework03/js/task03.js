let styles = ["Джаз", "Блюз"];

// 1 - add "Rock-n-roll" to end of stales
const a2 = "Рок-н-рол";
styles.push(a2);

//2 - find mediana of array
let pos = Math.floor(styles.length / 2);    //console.log(pos);
// and use splice for replace of mediana
const a3 = styles.splice(pos, 1, "Класика");
 
//3 - remove first element and show it
alert(`Ви видалили з масиву перший елемент: \n ${styles.shift()}` );

//4 - add two elements into the begin of styles
styles.unshift("Реп", "Реггі");
 
/функція-конструктор класу Human
function Human(name, name2, lastname, age, city){
    this.name = name;
    this.name2 = name2;
    this.lastname = lastname;
    this.age = age;
    this.city = city;
    this.createDate = Date().slice(0,25);  //для екземплярів класу Human
    
//властивість функції-конструктора Human - лічильник її викликів (статична властивість)    
    Human.getCount++;
}

//метод  add  функції-конструктора Human -  додає до масиву arr
Human.prototype.add = function(arr) {arr.push(this)};

//метод show для екземплярів класу Human
Human.prototype.show = function() {
    return `Картка користувача ${this.name} <br/>
    ПІБ: ${this.lastname} ${this.name}  ${this.name2} <br>
    Вік: ${this.age} <br>
    Місто: ${this.city}<br>
    Дата створення: ${this.createDate} <br><br>`;
}

//метод  isEmpty  функції-конструктора Human -  перевіряє чи не пустий обєкт
Human.prototype.isEmpty = function() {
    for (let proper in this){ return false};
        true;
}        
// метод add 
//Human.prototype.add = function (obj) {
//    userArr.push(obj);
//}

let userArr = [];

Human.getCount = 0;

const user1 = new Human("Іван", "Петрович", "Сніжко", "21", "Київ");
const user3 = new Human("Іван", "Йосипович", "Михалюк", "18", "Луцьк");
const user2 = new Human("Павло", "Семенович", "Корнійчук", "56", "Львів");
const user4 = new Human("Олег", "Олександрович", "Дубко", "35", "Рівне");
let userArr = [];
/*let userArr = [new Human("Іван", "Петрович", "Сніжко", "21", "Київ"), 
    new Human("Іван", "Йосипович", "Михалюк", "18", "Луцьк"),
    new Human("Олег", "Олександрович", "Дубко", "35", "Рівне"), 
    new Human("Олег", "Олександрович", "Дубко", "35", "Рівне")]; */
console.log(userArr);

if(!user1.isEmpty()){
    user1.add(userArr);
    document.write(user1.show());
}
if(!user2.isEmpty()){
    user2.add(userArr);
    document.write(user2.show());
}
if(!user3.isEmpty()){
    user3.add(userArr);
    document.write(user3.show());
}
if(!user4.isEmpty()){
    user4.add(userArr);
    document.write(user4.show());
}

document.write(`Всього користувачів: ${Human.getCount} `);

document.write(`<br> ${userArr.length} <br>`);


/*Реалізуйте програму перевірки телефону
   * Використовуючи JS Створіть поле для введення телефону та кнопку збереження.
   * Користувач повинен ввести номер телефону у форматі 000-000-00-00.
   * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера. 
   -Якщо номер правильний зробіть зелене тло і використовуючи document.location 
   переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
   -Якщо буде помилка, відобразіть її в діві до input.*/

   const div1 = document.querySelector(".container"),     //знаходимо div по класу
   divEr = document.createElement("div"),                 //створюємо div для виводу помилки
   inputTel = document.createElement("input"),            //створюємо input для вводу телефону
   btn = document.createElement("button"),                //створюємо button "Зберегти"
   pattern = /\b\d{3}-\d{3}-\d{2}-\d{2}\b/;              // визначаємо шаблон для перевірки tel
 
      divEr.innerText = "Неправильно введений номер!";    // створюємо повідомлення про помилку 
      divEr.classList.add("d-true");                      // додаємо клас для divEr
      div1.prepend(divEr);                                  // додаємо divEr в div-container

      inputTel.className = "input-tel";                     //додаємо клас для input 
      inputTel.placeholder = "000-000-00-00";              //підказка
      div1.append(inputTel);                               //додаємо input в div-container

      btn.className = "btn-save";                        //додаємо клас для button 
      btn.innerText = "Зберегти";                         //назва кнопки           
      div1.append(btn);                                  //додаємо button в div-container

btn.onclick = () =>{
   if(pattern.test(inputTel.value) ){     //true
      div1.classList.remove("container");
      div1.classList.add("true-number");                             // міняємо клас для div-container
      window.location.assign("https://simple.wikipedia.org/wiki/Telephone"); // переходимо на нову сторінку 
   // коли повертаємось на нашу сторінку -
      inputTel.value = "";                     //видаляємо попереднє значення 
      inputTel.placeholder = "000-000-00-00";  //відновлюємо підказку                 
   }else{                                //false
      divEr.classList.remove("d-true");           // з'являється повідомлення про помилку над input
      divEr.classList.add("d-false");
   }
}
// якщо клік мишкою на input
inputTel.onmousedown = () =>{
   inputTel.value = "";
   inputTel.placeholder = "000-000-00-00";  //відновлюємо підказку
   divEr.classList.remove("d-false");
   divEr.classList.add("d-true");
}

//window.location.reload (); обновить текущую страницу
//window.history.go (-1); вернуться на предыдущую страницу
//window.history.back (); вернуться на предыдущую страницу
//window.history.back (); После возврата он возвращает не только предыдущую страницу, 
//но и состояние предыдущей страницы.



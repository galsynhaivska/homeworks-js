//set document.location
export function setPageName(){
    const pageName = document.location.pathname; 
//    console.log(pageName);
    window.sessionStorage.pageName = pageName;
}
//click on navigation
export function setSelectGroup(){
    const [...productGroup] = document.querySelectorAll(".ul-header-item");
    const selectGroup = document.querySelector(".select-group");
    if(window.sessionStorage.productGroup){
        selectGroup.innerText = ` > ${window.sessionStorage.productGroup}`;
    }
    productGroup.forEach(elem => {
        elem.addEventListener("click", function(e) {
            if(e.target.innerText === elem.innerText){
                window.sessionStorage.productGroup = `${elem.innerText}`;
                window.sessionStorage.pageName = `${pathToItem.href}`;
            }         
        });
        
    });
}
//функція countCarts дає id карточкам!!!
export function countCarts(array){
    let i = 0;
    array.forEach(elem => {
        elem.id = i;
        i++;
    });
}

//функція createElement створює елемент і  його повертає!!!
export function createElement(elementName, className = "", content = "", link = ""){
    const element = document.createElement(elementName);
    if(className !== "" || className !== null){
        element.className = className;
    }
    if(content !== "" || content !== null){
        element.textContent = content;
    }
    if(link !== "" || link !== null){
        element.href = link;
    }
    return element   //повертається елемент     
}



//creating elements of card
export function createCart(itemResultName, i, divCards){
    const divCard = createElement("div", "card catalog-card"),
        divCardA = createElement("a", "a-card-item", '', "#"),
        divCardImg = createElement("img","img-card");
    divCardImg.src = `.${itemResultName.imageFront}`;
    divCardA.append(divCardImg);
    divCard.append(divCardA);

    const divCardTitle =  createElement("div", "card-title", `${itemResultName.itemName}` );
    divCard.append(divCardTitle);

    const  divCardStars =  createElement("div", "stars"),
        imgCardStars = createElement("img", "img-item");
        imgCardStars.src = "/image/stars.svg";
    divCardStars.append(imgCardStars);
    divCard.append(divCardStars);

    const pCard = createElement("p", "", "As low as "),
        spanCard =  createElement("span", "price-item", `$${(parseFloat(itemResultName.price)).toFixed(2)}`);
    pCard.append(spanCard);
    divCard.append(pCard);
    const divColors = createElement("div", "card-color");
    const itemsColor = JSON.parse(window.sessionStorage.itemsColor);
        for(let j = 0; j < itemsColor[1][i].length; j++){
        //    console.log(i, itemsColor[0][i][j], itemsColor[1][i][j], itemsColor[2][i][j], itemsColor[3][i][j]);
            const divItemColor = createElement("div", "item-card-color");
            divColors.append(divItemColor); 
            divItemColor.style.backgroundColor = `${itemsColor[2][i][j]}`;
            divItemColor.dataset.image = `${itemsColor[3][i][j]}`;
        }
    divCard.append(divColors);
    divCard.id = `${i}`;
//    console.log(divCard);
    const divBtn = createElement("div", "btn"),
        divBtnA = document.createElement("a", "", "ADD TO CART", "#");
    divBtnA.classList.add("card-btn-a");
    divBtnA.innerText = "ADD TO CART";
    divBtn.append(divBtnA);
    divCard.append(divBtn);
// add cart to page Catalog
    divCards.append(divCard);
    divCards.classList.add("catalog");
}

 //select items by size in resultName by function selectItem  
export function selectItem(keyItem, el){
    let array = [], res = [];
    array = JSON.parse(window.sessionStorage.resultName);
    for(let i = 0; i < array.length; i++){        
        for(let j = 0; j < array[i].length; j++){  
            if(keyItem === 'color'){
                if(array[i][j][keyItem] === el.dataset.color){
                    res.push(array[i][j]);
                } 
            }else if(keyItem === 'brand'){
        //        console.log(el.innerText);
        //        console.log(array[i][j][keyItem]);
                if(array[i][j][keyItem].includes(el.innerText)){
                    res.push(array[i][j]);
                } 
            }else{   
                if(array[i][j][keyItem] === el.innerText){
                    res.push(array[i][j]);
                } 
            }  
        }
    }
    window.sessionStorage.resultSelect = JSON.stringify(res);
    return res;
}

export function groupItem(array){
    let result = array.reduce((r, a) =>{
        r[a.itemName] =  r[a.itemName] || [];
        r[a.itemName].push(a);
        return r;
    }, {});
    let arrGroup = [];
    for (let key in result){
        arrGroup.push(result[key]);
    }
    return arrGroup;
}



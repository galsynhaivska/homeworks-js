import {req, show, reqKurs, showKurs, showStars, reqStars } from "./function.js"
/* 
Зробити програму з навігаційним меню,
    яке буде показувати один з варіантів. 
  - Курс валют НБУ з датою на який день https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json, 
  - героїв зоряних війн https://swapi.dev/api/people/, 
  - список справ з https://jsonplaceholder.typicode.com/
   виводити які виконані та які ні, з можливістю редагування
*/

if(localStorage.history === undefined){
    localStorage.history = JSON.stringify([]);
}

document.getElementById("kurs").addEventListener("click", () => {
    reqKurs("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
    .then(info => showKurs(info));
//    .then(info => console.log(info));
    document.querySelector(".loader").classList.remove("active");
});

document.getElementById("stars").addEventListener("click", () => {
    reqStars("https://swapi.dev/api/people")
    .then(info => showStars(info));
//    .then(info => console.log(info))
    document.querySelector(".loader").classList.remove("active");
});


document.getElementById("todo").addEventListener("click", () =>{
    req("https://jsonplaceholder.typicode.com/todos")
    .then(info => show(info));
//    .then(info => console.log(info));
    document.querySelector(".loader").classList.remove("active");
});


document.querySelector("#close")
.addEventListener("click", e => 
    document.querySelector(".parent").classList.remove("active"));






const userName = prompt("Ваше імя?", 'Іван');
const userAge = prompt("Ваш вік?", '50');
const userCity = prompt("Ваше місто проживання?",'Київ');
// const userData is created for  output user data in <p class="user-data"> 
const userData = "Ім'я: "+ userName + "<br>"+ 'Вік: ' + userAge + "<br>"+ 'Місто проживання: ' + userCity;

//output user data in console
console.log("Користувач");
console.log(`Ім'я: ${userName}`);
console.log(`Вік: ${userAge}`);
console.log(`Місто проживання: ${userCity}`);
console.log("Кінець виводу");

//***output userdata in <p class="user-data"> ==> as result we will fill this <p> with user data
document.write('<p class="user-data">'+ userData + "</p>");

//output user data in <div> ==> as result we will have three <div>
document.write("<div>"+ 'Користувач' + "</div>");
document.write("<div>"+ "Ім'я: " + userName + "</div>");
document.write("<div>"+ 'Вік: ' + userAge + "</div>");
document.write("<div>"+ 'Місто: ' + userCity + "</div>");

//output user data in <p class="user-data"> ==> as result we will have three <p>
//document.write("<p class="user-data">"+ 'Користувач' + "</p>");
document.write('<p class="user-data">'+ "Ім'я: " + userName + "</p>");
document.write('<p class="user-data">'+ 'Вік: ' + userAge + "</p>");
document.write('<p class="user-data">'+ 'Місто: ' + userCity + "</p>");

//***output user data in <p class="user-data"> ==> as result we will have one <p>
document.write('<p class="user-data">'+ "Ім'я: " + userName + "<br>" + 'Вік: ' + userAge +
"<br>" + 'Місто: ' + userCity + "</p>");




export const cakes = [
    {
        id: "1",
        cakeName: "Carrot",
        image:"carrot.png",
        quantity: 15,
        price: "3.49",
        info: "Walnut-studded carrot cake with cinnamon cream cheese frosting"
    },
    {
        id: "2",
        cakeName: "RedVelvet",
        image:"red-velvet.png",
        quantity: 10,
        price: "5.99",
        info: "A chocolate sponge, coloured naturally with beetroot and topped off with cream cheese."
    },
    {
        id: "3",
        cakeName: "Mint Chip",
        image:"mint-chip.png",
        quantity: 10,
        price: "5.49",
        info: "Homemade chocolate cupcakes topped with thick & creamy mint chocolate chip frosting."
    },
    {
        id: "4",
        cakeName: "Pink strawberry",
        image:"strawberry.png",
        quantity: 15,
        price: "5.49",
        info: "A fluffy strawberry cupcake with strawberry buttercream frosting and chocolate syrup."
    },
    {
        id: "5",
        cakeName: "Marshmallow",
        image:"marshmallow.png",
        quantity: 15,
        price: "3.99",
        info: "A super chocolatey cupcake base with a soft marshmallowy buttercream topping."
    },
    {
        id: "6",
        cakeName: "Dark chocolate",
        image:"dark-chocolate.png",
        quantity: 10,
        price: "4.99",
        info: "Belgian chocolate cake with sweet chocolate frosting and cheery."
    },
    {
        id: "7",
        cakeName: "Salty Caramel",
        image:"salty-caramel.png",
        quantity: 15,
        price: "4.49",
        info: "Caramel cake with a buttery caramel cream cheese frosting topped with fleur de sel."
    },
    {
        id: "8",
        cakeName: "Gluten Free Velvet",
        image:"gluten-free-velvet.png",
        quantity: 12,
        price: "5.99",
        info: "A gluten free twist on our classic red velvet, topped off with cream cheese."
    },
    {
        id: "9",
        cakeName: "Cinnamon",
        image:"cinnamon.png",
        quantity: 15,
        price: "3.49",
        info: "Lightly spiced buttermilk cake with cinnamon cream cheese frosting with cinnamon sugar."
    },
    {
        id: "10",
        cakeName: "Totally nuts",
        image:"totally-nuts.png",
        quantity: 15,
        price: "4.49",
        info: "A sweet hazelnut paste with nutella butter cream and chopped hazelnuts on the top."
    },
    {
        id: "11",
        cakeName: "Pink Vanilla",
        image:"pink-vanilla.png",
        quantity: 15,
        price: "3.49",
        info: "Soft, fluffy, and extra moist creamy vanilla buttercream with extra sprinkles."
    }    
];